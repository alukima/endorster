class Lonk
	attr_accessor :from, :to, :multiplicity
	def initialize(from, to)
		@from = from
		@to = to
		@multiplicity = 1
	end
	def to_s
		st = from.to_s + ">" + to.to_s
		return st
	end
	def nominal_value
		return from.trust_for_user(to)
	end
	def value
		return nominal_value / multiplicity
	end
end

class Chain
	attr_accessor :link1, :link2
	def initialize(link1, link2)
		@link1 = link1
		@link2 = link2
	end
	def to_s
		st = "CHAIN(" + link1.to_s + "," + link2.to_s + ")"
		return st
	end
	def value
		x = link1.value
		y = link2.value
		if x > 0
			if x == Float::INFINITY or y == Float::INFINITY
				return [x,y].min
			else
				return 100.0*Math.log( (1+Math.exp((-x-y)/100.0))/(Math.exp(-x/100.0)+Math.exp(-y/100.0)) )
			end
		else
			return 0.0
		end
	end
end

class Parallel
	attr_accessor :links
	def initialize(links)
		@links = links
	end
	def to_s
		st = "PARA("
		for link in links do
			st += (link.to_s+",")
		end
		st = st[0..-2]+")"
		return st
	end
	def value
		sum = 0.0
		for link in links
			sum += link.value
		end
		return sum
	end
end

def prioritize_paths(list_of_paths)
	first = list_of_paths[0][0]
	last = list_of_paths[0][-1]
	output = []
	buckets = {}

	for path in list_of_paths do
		if path[1] == last
		output.push(Lonk.new(path[0],path[1]))
		else
			if buckets[path[1]].nil?
				buckets[path[1]] = [path[1..-1]]
			else
				buckets[path[1]].push(path[1..-1])
			end
		end
	end

	puts buckets
	puts output
	
	buckets.each_key do |buck|
		x = prioritize_paths(buckets[buck])
		y = Chain.new(Lonk.new(first,buck), x)
		output.push(y)
	end
	if output.length == 1
		output = output[0]
	else
		output = Parallel.new(output)
	end
	return output
end


def count_links(structure)
	if Lonk === structure
		output = {structure => 1}
	elsif Chain === structure
		output = {}
		count1 = count_links(structure.link1)
		count2 = count_links(structure.link2)
		for count in [count1,count2]
			count.each_key do |link|
				if output[link].nil?
					output[link] = count[link]
				else
					output[link] += count[link]
				end
			end
		end
	elsif Parallel === structure
		output = {}
		for sub in structure.links
			count = count_links(sub)
			count.each_key do |link|
				if output[link].nil?
					output[link] = count[link]
				else
					output[link] += count[link]
				end
			end
		end
	end
	return output
end

def assign_multiplicity(structure, count)
	if Lonk === structure
		structure.multiplicity = count[structure]
	elsif Chain === structure
		assign_multiplicity(structure.link1, count)
		assign_multiplicity(structure.link2, count)
	elsif Parallel === structure
		for sub in structure.links
			assign_multiplicity(sub, count)
		end
	else
		puts "WHAT IS THIS YOU'RE TRYING TO ASSIGN MULTIPLICITY TO?"
	end
end


def compute_score(list_of_paths)
	structure = prioritize_paths(list_of_paths)
	count = count_links(structure)
	assign_multiplicity(structure, count)
	return structure.value
end


#puts prioritize_paths([['a','b','c'],['a','b','d','c'],['a','d','c']])
#puts prioritize_paths([['a','b','c','d','e','f','g','h']])

#Para(Chain(a>b, Para(b>c, Chain(b>d, d>c))), a>c)


#PARA(["a", "c"],CHAIN(a>b,PARA(["b", "c"],CHAIN(b>d,["d", "c"]))))


commentariat = """
class PrioritizePaths
	def self.compose(x, y)
		if x > 0
			if x == Float::INFINITY or y == Float::INFINITY
				return [x,y].min
			else
				return 100.0*Math.log( (1+Math.exp((-x-y)/100.0))/(Math.exp(-x/100.0)+Math.exp(-y/100.0)) )
			end
		else
			return 0.0
		end
	end

	def self.link(a, b)
		a.trust_for_user(b)
	end
		
	def self.chain(path)
		if path.length == 1
			return Float::INFINITY
		elsif path.length == 2
			return link(path[0], path[1])
		else
			return compose(chain(path[0..-2]), link(path[-2], path[-1]))
		end
	end

	def self.equals_path(path_a, path_b)
		if path_a.length != path_b.length
			return false
		else
			path_a.each_with_index do |aa,i|
				if aa != path_b[i]
					return false
				end
			end
			return true
		end
	end

	def self.already_in(path, list_of_paths)
		list_of_paths.each do |p|
			if equals_path(path,p)
				return true
			end
		end
		return false
	end
			

	def self.compute_score(list_of_paths)
		if list_of_paths.length == 0
			return 0.0
		elsif list_of_paths.length == 1
			return chain(list_of_paths[0])
		else
			last = list_of_paths[0][-1]
			list_of_pens = []
			list_of_paths.each do |path|
				pen = path[-2]
				if !list_of_pens.include? pen
					list_of_pens << pen
				end
			end
			running_sum = 0
			list_of_pens.each do |pen|
				pen_path_list = []
				list_of_paths.each do |path|
					path.each_with_index do |pathi,i|
						if pathi == pen
							path_to_pen = path[0..i]
							if !already_in(path_to_pen, pen_path_list)
								pen_path_list << path_to_pen
							end
							break
						end
					end
				end
				t = compute_score(pen_path_list)
				s = compose(t,link(pen,last))
				running_sum += s
			end
			return running_sum
		end
	end
end
"""