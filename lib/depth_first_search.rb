require 'prioritize_paths'

class DepthFirstSearch
	@@output = []
	def self.find_paths(source, target)
		@@output = []
		target_path = r_user(source, target, [])
		return @@output
	end
	
	def self.r_user(user, target, chain)
		if user == target
			chain << user
			@@output << chain
			return chain
		end

		if chain.include? user
			return nil 
		end
		trustees = user.trustees
		if trustees
			trustees.each do |trusts|
				new_chain = chain.clone
				r_user(trusts, target, new_chain << user)
			end
		end
	end

	def self.score(source, target)
		if source.master
			source = source.master
		end
		if target.master
			target = target.master
		end
		puts "SOURCE"+source.username
		puts "TARGET"+target.username
		if source==target
			return nil
		else
			list_of_paths = find_paths(source, target)
			puts "LIST OF PATHS!", list_of_paths, list_of_paths.length
			if list_of_paths && list_of_paths.length > 0
				return compute_score(list_of_paths)
			else
				return 0
			end
		end
	end
end
