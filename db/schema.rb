# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 201207311234567) do

  create_table "association_requests", :force => true do |t|
    t.integer  "from_id"
    t.integer  "to_id"
    t.boolean  "is_from_master_to_facet"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  create_table "connections", :force => true do |t|
    t.string   "website_name"
    t.string   "website_url"
    t.string   "account_name"
    t.string   "homepage_url"
    t.string   "unique_token"
    t.boolean  "write_permission"
    t.boolean  "public"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.integer  "user_id"
  end

  create_table "hipster_ratings", :force => true do |t|
    t.datetime "dateopened"
    t.datetime "dateclosed"
    t.integer  "ratee_id"
    t.integer  "rater_id"
    t.integer  "intermediary_id"
    t.integer  "weight"
    t.integer  "value"
    t.string   "comment"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  add_index "hipster_ratings", ["intermediary_id"], :name => "index_hipster_ratings_on_intermediary_id"
  add_index "hipster_ratings", ["ratee_id"], :name => "index_hipster_ratings_on_ratee_id"
  add_index "hipster_ratings", ["rater_id"], :name => "index_hipster_ratings_on_rater_id"

  create_table "links", :force => true do |t|
    t.integer  "user_id"
    t.text     "description"
    t.string   "unique_token"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "mass_emails", :force => true do |t|
    t.string   "invitees"
    t.integer  "lowerlimit"
    t.integer  "upperlimit"
    t.boolean  "unlimited"
    t.string   "subject"
    t.string   "message"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "notifications", :force => true do |t|
    t.string   "text"
    t.string   "base_url"
    t.string   "thing_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "user_id"
    t.boolean  "read"
  end

  create_table "ratings", :force => true do |t|
    t.datetime "dateopened"
    t.datetime "dateclosed"
    t.integer  "ratee_id"
    t.integer  "rater_id"
    t.integer  "value"
    t.string   "comment"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.boolean  "unlimited"
    t.integer  "lowerlimit"
    t.integer  "upperlimit"
    t.string   "unique_token"
    t.text     "review"
    t.integer  "referrer_id"
  end

  add_index "ratings", ["ratee_id"], :name => "index_ratings_on_ratee_id"
  add_index "ratings", ["rater_id"], :name => "index_ratings_on_rater_id"

  create_table "referrers", :force => true do |t|
    t.string   "name"
    t.string   "hashed_key"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
    t.string   "title"
    t.string   "url"
    t.boolean  "secure_authentication"
    t.text     "public_key"
    t.integer  "next_sequence"
  end

  create_table "users", :force => true do |t|
    t.string   "username"
    t.string   "crypted_password"
    t.string   "password_salt"
    t.string   "persistence_token"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
    t.string   "email"
    t.string   "pretty_name"
    t.text     "authorized_referrers"
    t.boolean  "email_verified"
    t.string   "code"
    t.integer  "master_id"
    t.boolean  "inactive"
    t.string   "hashed_activation_code"
    t.text     "description"
  end

end
