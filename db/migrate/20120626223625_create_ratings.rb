class CreateRatings < ActiveRecord::Migration
  def change
    create_table :ratings do |t|
      t.datetime :dateopened
      t.datetime :dateclosed
      t.references :ratee
      t.references :rater
      t.integer :weight
      t.integer :value
      t.string :comment

      t.timestamps
    end
    add_index :ratings, :ratee_id
    add_index :ratings, :rater_id
  end
end
