class AddUniqueTokenToRating < ActiveRecord::Migration
  def change
		add_column :ratings, :unique_token, :string
  end
end
