class RemoveReferrerIdFromUsers < ActiveRecord::Migration
  def up
    remove_column :users, :referrer_id
      end

  def down
    add_column :users, :referrer_id, :integer
  end
end
