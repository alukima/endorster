class AddInfoToReferrers < ActiveRecord::Migration
  def change
    add_column :referrers, :title, :string

    add_column :referrers, :url, :string

    add_column :referrers, :created_date, :datetime

    add_column :referrers, :secure_authentication, :boolean

    add_column :referrers, :public_key, :text

    add_column :referrers, :next_sequence, :integer

  end
end
