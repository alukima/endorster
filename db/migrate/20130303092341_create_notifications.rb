class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.string :text
      t.string :base_url
      t.string :thing_id

      t.timestamps
    end
  end
end
