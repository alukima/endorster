class AddMasterIdToUsers < ActiveRecord::Migration
  def up
	add_column :users, :master_id, :integer, :null=>true
  end

  def down
	remove_column :users, :master_id
  end
end


