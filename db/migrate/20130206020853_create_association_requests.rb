class CreateAssociationRequests < ActiveRecord::Migration
  def change
    create_table :association_requests do |t|
      t.integer :from_id
      t.integer :to_id
      t.boolean :is_from_master_to_facet

      t.timestamps
    end
  end
end
