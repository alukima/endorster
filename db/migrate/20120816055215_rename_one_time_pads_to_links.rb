class RenameOneTimePadsToLinks < ActiveRecord::Migration
  def up
		rename_table :one_time_pads, :links
  end

  def down
		rename_table :links, :one_time_pads
  end
end
