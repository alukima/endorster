class AddEmailVerifiedToUsers < ActiveRecord::Migration
  def up
	add_column :users, :email_verified, :boolean
  end

  def down
  	remove_column :users, :email_verified
  end
end
