class AddUnlimitedField < ActiveRecord::Migration
  def up
	add_column :ratings, :unlimited, :boolean
	add_column :ratings, :lowerlimit, :integer
	add_column :ratings, :upperlimit, :integer
	remove_column :ratings, :weight
  end

  def down
  	remove_column :ratings, :unlimited
	remove_column :ratings, :lowerlimit
	remove_column :ratings, :upperlimit
	add_column :ratings, :weight, :integer
  end
end
