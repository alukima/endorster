class CreateMassEmails < ActiveRecord::Migration
  def change
    create_table :mass_emails do |t|
      t.string :invitees
      t.integer :lowerlimit
      t.integer :upperlimit
      t.boolean :unlimited
      t.string :subject
      t.string :message

      t.timestamps
    end
  end
end
