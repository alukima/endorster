class RenamePrivilegeTokens < ActiveRecord::Migration
  def change
    rename_column :users, :privilege_tokens, :authorized_referrers
  end
end