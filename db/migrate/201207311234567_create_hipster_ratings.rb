class CreateHipsterRatings < ActiveRecord::Migration
  def change
    create_table :hipster_ratings do |t|
      t.datetime :dateopened
      t.datetime :dateclosed
      t.references :ratee
      t.references :rater
      t.references :intermediary
      t.integer :weight
      t.integer :value
      t.string :comment

      t.timestamps
    end
    add_index :hipster_ratings, :ratee_id
    add_index :hipster_ratings, :rater_id
    add_index :hipster_ratings, :intermediary_id
  end
end
