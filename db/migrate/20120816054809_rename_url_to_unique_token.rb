class RenameUrlToUniqueToken < ActiveRecord::Migration
  def up
		rename_column :one_time_pads, :url, :unique_token
  end

  def down
		rename_column :one_time_pads, :unique_token, :url
  end
end
