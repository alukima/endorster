class CreateReferrers < ActiveRecord::Migration
  def change
    create_table :referrers do |t|
      t.string :name
      t.string :hashed_key

      t.timestamps
    end
  end
end
