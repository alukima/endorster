class RemoveCreatedDateFromReferrers < ActiveRecord::Migration
  def up
    remove_column :referrers, :created_date
      end

  def down
    add_column :referrers, :created_date, :datetime
  end
end
