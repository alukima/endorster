class AddActivationToUsers < ActiveRecord::Migration
  def change
    add_column :users, :inactive, :boolean

    add_column :users, :hashed_activation_code, :string

  end
end
