class AddPrivilegeTokensToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :privilege_tokens, :text
  end 

  def self.down
    remove_column :users, :privilege_tokens
  end

end
