class CreateConnections < ActiveRecord::Migration
  def change
    create_table :connections do |t|
      t.string :website_name
      t.string :website_url
      t.string :account_name
      t.string :homepage_url
      t.string :unique_token
      t.boolean :write_permission
      t.boolean :public

      t.timestamps
    end
  end
end
