class CreateOneTimePads < ActiveRecord::Migration
  def change
    create_table :one_time_pads do |t|
      t.integer :user_id
      t.text :description
      t.string :url

      t.timestamps
    end
  end
end
