class AddEmailToUsers < ActiveRecord::Migration
  def up
	add_column :users, :email, :string
	add_column :users, :pretty_name, :string
  end

  def down
	remove_column :users, :email, :string
	remove_column :users, :pretty_name, :string
  end
end
