module ApplicationHelper

	#Create a random string of 18 characters.
	def random_token
		SecureRandom::urlsafe_base64(18)
	end
	
	#Given a dictionary of arguments, and a list of required arguments,
	#return a stringified list of the ones (if any) that are missing.
	def missing_arguments(dict, required_arguments)
		output = ""
		required_arguments.each do |arg|
			if dict[arg].nil?
				output += (arg + ", ")
			end
		end
		if output == ""
			return false
		else
			output = output[0..-3]
			return output
		end
	end
	
	# Set the values for the notification, awaiting, and outstanding bubbles.
	def bubbles
		if (current_user)
			@awaiting = Rating.where("rater_id = ?", current_user.id).where("dateclosed IS NULL").count
			#@outstanding = Rating.where("ratee_id = ?", current_user.id).where("dateclosed IS NULL").count
		end
	end
	
	def unread
    @notifications = Notification.where("user_id = ?", current_user.id).where("read = ?", false)
	end
	
	def display_score(score)
		if score.nil?
			return "&infin;"
		end
		if (score > 0)
			sign = "+"
		elsif (score < 0)
			sign = "&minus;"
			score = -score
		else
			return "0"
		end
		number = score.round #(score * 100).round.to_f / 100
		return sign+number.to_s
	end

	
	def color_for_score(score)
		if score.nil?
			return "fff"
		end
		red = "ff"
		green = "ff"
		prob = 1/(1+Math.exp(-score/100.0))
		if prob < 0.5
			green = (256*prob+127).round.to_s(16)
			if green.length == 1
				green = "0"+green
			end
		elsif prob > 0.5
			red = (383-256*prob).round.to_s(16)
			if red.length == 1
				red = "0"+red
			end
		end
		blue = "80"
		return red+green+blue
	end

	def date_span(date, pretext="", css_class="date")
		now = Time.now
		s = (now - date).floor
		q = false
		if s < 1
			n = 0
			u = "moment"
		elsif s < 60
			n = s
			u = "second"
		elsif s < 3600
			n = s/60
			u = "minute"
		elsif s < 86400
			n = s/3600
			u = "hour"
		elsif s < 604800
			n = s/86400
			u = "day"
		elsif s < 2592000
			n = s/604800
			u = "week"
		elsif s < 31557807 #seconds in a year
			n = s/648000 #quarter months
			u = "month"
			q = true 
		else
			n = s/7889452 #quarter years
			u = "year"
			q = true
		end
		if q
			ww = n/4
			qq = n%4
			case qq
			when 0
				quarters = ""
			when 1
				quarters = "&frac14;"
			when 2
				quarters = "&frac12;"
			when 3
				quarters = "&frac34;"
			end
			number = ww.to_s + quarters
			unit = u + (ww==1 && qq==0 ? "" : "s")
		else
			number = n==0 ? "" : n.to_s
			unit = u + (n==1 ? "" : "s")
		end
		return ('<span class="'+css_class+'" title="'+date.to_s+'">'+pretext+number+' '+unit+' ago</span>')
	end
end
