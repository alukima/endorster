class MyMailer < ActionMailer::Base
  default from: "noreply@endorster.net"



  def verificationmail(address,pretty_name,code)
    mail(
      :to      => address,
      :from    => "noreply@endorster.net",
      :subject => "Endorster email verification",
      :body    => "Hello "+pretty_name+",\n\nThank you for verifying your email address! Follow this link to complete verification: www.endorster.net/v/"+code
    )
  end


  def invitationemail(from,to,subject,message)
    mail(
      :to      => to,
      :from    => "noreply@endorster.net",
      :reply_to=> from,
      :subject => subject,
      :body    => message
    )
  end


end
