<!-- /about -->

<h1>What is Endorster?</h1>

<p><b><i>Endorster is a reputation network that makes it easier for you to do business online.</i></b></p>

<ol>
	<li><b><a href="#1">Overview</a></b></li>
	<ul>
		<li><a href="#1.1">Introduction</a></li>
		<li><a href="#1.2">Why should I use Endorster?</a></li>
		<li><a href="#1.3">How do I get started?</a></li>
		<li><a href="#1.4">Is it really that easy? You don't want my email/etc.?</a></li>
		<li><a href="#1.5">Okay, I'm in. How do I rate someone?</a></li>
	</ul>
	<li><b><a href="#2">Reputation scores</a></b></li>
	<ul>
		<li><a href="#2.1">What is a "score"?</a></li>
		<li><a href="#2.2">Wait, I'm confused. How can I find out <i>my</i> score?</a></li>
		<li><a href="#2.3">How are scores calculated?</a></li>
		<li><a href="#2.4">What is "hipster cred"?</a></li>
	</ul>
	<li><b><a href="#3">Using Endorster</a></b></li>
		<ul>
		<li><a href="#3.1">How do I send and accept rating invitations?</a></li>
		<li><a href="#3.2">What are "invitation limits"?</a></li>
		<li><a href="#3.3">What do these numbers mean?</a></li>
		<li><a href="#3.4">What are identity links and sharing links?</a></li>
		<li><a href="#3.5">What is a "verifiable link"?</a></li>
	</ul>
	<li><b><a href="#4">Contact us</a></b></li>
	<ul>
		<li><a href="#4.1">General inquiries</a></li>
		<li><a href="#4.2">Can I use Endorster with my online community?</a></li>
	</ul>
</ol>


<h2><a name="1" class="normal">Overview</a></h2>

<h3><a name="1.1" class="normal">Introduction</a></h3>
<p>Have you ever done business with a stranger on the internet? 
Buying or selling, renting or lending, lodging or hosting, with someone you've never met before?</p>

<p>This is a great way to meet people and save money. But when it doesn't work out,
things can take a turn for the worse. How can you trust that you're dealing with good, reliable people?
How can you convince <i>them</i> to trust <i>you</i>?</p>

<p><b>Endorster helps solve this problem.</b> With Endorster, you can check other people's ratings before doing
business with them, and assign them a rating after you're done. On the other side, you can invite 
someone to rate you beforehand, so that they see you're putting your reputation at stake.</p>

<h3><a name="1.2" class="normal">Why should I use Endorster?</a></h3>
<ul>
	<li><b>Endorster is universal.</b> You can use your Endorster account to establish and build trust anywhere,
		whether online or offline, whatever you need it for.</li>
	<li><b>Endorster helps you trust and be trusted by other people.</b> Knowing the broad benefits of a good 
	reputation, those you interact with will see that an invitation to rate you really <i>means</i>
	something.</li>
	<li><b>Endorster puts your reputation in your hands.</b> There's no mystery in how we calculate your scores.
		You have complete control over who can rate you and how strongly. Your reputation is not just a list of
		facts about you: it's an asset that's yours to use.</li>
</ul>

<h3><a name="1.3" class="normal">How do I get started?</a></h3>
<p>Signing up is easy: just click <a href="/signup">Sign up</a> and select your username and password.</p>

<h3><a name="1.4" class="normal">Is it really that easy? You don't want my email/etc.?</a></h3>
<p>It really is. You can provide your email if you'd like to get notifications, but we don't require it, and it won't affect your ratings.</p>
<p>You are also entirely free to create multiple accounts for yourself &mdash; it won't affect your reputation
score either. (See <a href="#2.3"><i>How are scores calculated?</i></a>)</p>

<h3><a name="1.5" class="normal">Okay, I'm in. How do I rate someone?</a></h3>
<p>You can't rate someone until they've invited you to rate them. For more information, see 
<a href="#3.1"><i>How do I send and accept rating invitations?</i></a></p>


<h2><a name="2" class="normal">Reputation scores</a></h2>

<h3><a name="2.1" class="normal">What is a "score"?</a></h3>
<p>When you're logged in and look at someone's profile, you'll see "Your score of (user)" and
"(user)'s score of you."</p>
<p>The score represents how much we've calculated you should trust someone. A score of 0 means that you don't know
anything about them at all. A <b>positive</b> score means you <i>can</i> trust them &mdash; the higher the score, 
the more you can trust them. A <b>negative</b> score means you might be taking a risk.</p>
<p>You can also see other people's scores of you, so you know what we're telling other people about you!</p>

<h3><a name="2.2" class="normal">Wait, I'm confused. How can I find out <i>my</i> score?</a></h3>
<p>You don't have one <i>single score</i>. Rather, your score depends on who's looking at you. This is because the
score represents your <i>relationships</i> with other people. People who know you can trust you more than
people who don't, even though you're the same person to all of them.</p>

<h3><a name="2.3" class="normal">How are scores calculated?</a></h3>
<p>Scores are calculated from two sources. The first is from <b>chains of ratings</b>. If Alice trusts Bob and Bob
trusts Carol, then we know that Alice should trust Carol at least a little bit. On the other hand, if Alice trusts
Bob and Bob <i>distrusts</i> Carol, then Alice should likewise distrust Carol a little bit. The shorter the chain,
and the larger the ratings, the more of an impact the chain has on the score.</p>

<h3><a name="2.4" class="normal">What is "hipster cred"?</a></h3>
<p>The second source of information for scores is <b>hipster cred</b>. This is our highly technical term for 
<i>how accurate your ratings have turned out to be</i>. In
other words, if you like somebody "before they're cool," then people can see that your rating was accurate, so
they now trust you a little more. On the other hand, you can get <i>negative</i> hipster cred if you rate someone
<i>inaccurately</i>, so think before you rate!</p>


<h2><a name="3" class="normal">Using Endorster</a></h2>

<h3><a name="3.1" class="normal">How do I send and accept rating invitations?</a></h3>
<p>You can send rating invitations using the "invite ratings" feature on the dashboard after you log in. (See the
<a href="/help/">Dashboard Help</a> for more details.) You can invite another Endorster user, or you can send an
"open invitation" to someone who isn't yet a member. When they follow the link you send them, they will be 
prompted to create an account to accept your invitation.</p>

<p>If someone directly invites your Endorster account, there's no need to accept the invitation explicitly; it's
automatically added to your "confirm ratings" inbox to await your rating, which you can then assign at your leisure.</p>

<h3><a name="3.2" class="normal">What are "invitation limits"?</a></h3>
<p><b>Invitation limits</b> are the minimum and maximum ratings with which you'll allow someone to rate you. You
don't have to include limits, but if you do, the lower limit must be negative and the upper limit must be positive.
In other words, you need to <i>put something at stake</i> in order to invite a rating.</p>

<p><b>The larger the negative limit is compared to the positive limit,</b> the <b>more</b> you should trust the
person who invited you. If that seems strange, think of it this way: Someone who offers you limits of
(&ndash;200,+10) has a lot more to lose from being mean to you than someone who offers (&ndash;10,+200).</p>

<h3><a name="3.3" class="normal">What do these numbers mean?</a></h3>
<p>A good rule of thumb when assigning a rating is to treat it like a 5-star rating: rating the maximum is like
5 stars, rating the minimum is like 1 star, and rating 0 is like 3 stars. You can adjust your rating in between
to whatever you like.</p>

<p>If your issuing an invitation, here are some rough suggestions for limits to give people:</p>

<ul>
	<li><b>You're giving someone a ride:</b> (&ndash;50,+20)</li>
	<li><b>You're staying in someone's home:</b> (&ndash;200,+60)</li>
	<li><b>You're babysitting for someone:</b> (&ndash;500,+50)</li>
</ul>

<p>(You can also use these as a guideline if someone has given you an unlimited invitation.)

<p>All ratings are ultimately yours to decide. But remember: choose carefully, so that you'll get good
hipster cred!</p>

<!--
<h3>Really, what do these numbers mean? What are the "units of trust"?</h3>
<p>If you want to know badly enough to read the source code...</p>
<p>To be completely precise, a rating of <i>N</i> corresponds to a probability of 1/(1+e<sup>-<i>N</i></sup>) that
somebody is good.</p>
-->

<h3><a name="3.4" class="normal">What are identity links and sharing links?</a></h3>
<p><b>Identity links</b> and <b>shares</b> are features that you can use to connect your Endorster account to some other identity. Identity links point to other accounts of yours elsewhere on the internet, and are displayed on your public profile under the "Identities" header. Shares, by contrast, are seen only by people to whom you send the unique link. You can create a share to confirm that you are the owner of a particular Endorster account without cluttering up your Identities with one-time-use links.</p>


<h3><a name="3.5" class="normal">What is a "verifiable link"?</a></h3>
<p>A <b>verifiable link</b> is an identity link that points to something that itself points back to your Endorster account. For example, you could link to a posting or profile on another website that contains a statement "My Endorster account is (whatever)".</p>

<p>We recommend that you make all of your identity links verifiable if possible. Otherwise, someone who's looking at your Endorster profile won't know whether you actually are the same person as the links indicate, or whether you're deceptively linking to other accounts that aren't yours.</p>

<a name="4" class="normal"><h2>Contact us</h2></a>

<h3><a name="4.1" class="normal">General inquiries</a></h3>
<p>Problems? Questions? Get in touch with us at <b>info@endorster.net</b>.</p>

<h3><a name="4.2" class="normal">Can I use Endorster with my online community?</a></h3>
<p>If you're running an online community where trust is important, Endorster reputation can help more people to join your community, and existing members to become more active. We can integrate with your platform so that your users can seamlessly link their accounts to Endorster, or create new Endorster accounts just for your site. Please <a href="mailto:info@endorster.net">email</a> for more information.</p>

<p>We also have a <a href="/api/docs">public API</a> which is available for testing, if you would like to see how Endorster can work.
