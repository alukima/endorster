require 'securerandom'

class Link < ActiveRecord::Base
	belongs_to :user
	def self.create_hash
		SecureRandom::base64(18).gsub('+','-').gsub('/','_')
	end

	def show_url
		base = "http://localhost:3000/" #"http://warm-autumn-2844.herokuapp.com/"
		path = "links/"
		base + path + unique_token
	end
	
	def title
		if description == ""
			return "(untitled)"
		else
			return description
		end
	end
	
	def short_description
		if description.length > 22
			description[0..19] + "..."
		else
			description
		end
	end
end
