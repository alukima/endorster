class User < ActiveRecord::Base
	acts_as_authentic do |c|
		c.validate_email_field = false
	end
  has_many :notifications
	has_many :outgoingratings, :class_name => 'Rating', :foreign_key => 'ratee_id'
	has_many :incomingratings, :class_name => 'Rating', :foreign_key => 'rater_id'
	has_many :connections
	has_many :links
	belongs_to :master, :class_name => 'User'
	has_many :facets, :class_name => 'User', :foreign_key => "master_id"
	attr_accessible :master, :facets, :username, :pretty_name, :email, :password, :password_confirmation, :description
	has_many :sent_requests, :class_name => 'AssociationRequest', :foreign_key => 'from_id'
	validates_format_of :username, :with => /^[A-Za-z\d_]+$/
	validates :username, :length => { :minimum => 3, :maximum => 30 }
	validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :create, :allow_blank => true
	serialize :authorized_referrers, Hash


	def valid_password?(password)
		if inactive
			return false
		else
			return super(password)
		end
	end

	def to_param
		pretty_name
	end
	def to_s
		pretty_name
	end
	
	def trustees
		#ratings = Rating.where("rater_id = ?", id).where("dateclosed IS NOT NULL")
		
		rater_facets = []
		if master or facets
			the_master = (master ? master : self)
			rater_facets << the_master.id
			the_master.facets.each do |facet|
				rater_facets << facet.id
			end
		else
			rater_facets << id
		end
		
		#Test 1.1
		if rater_facets.include? id
			puts "GOOD!1.1"
		else
			raise "rater_facets did not contain my own id!.1"
		end
		rater_facets_string = rater_facets.join(",")			
		ratings = Rating.where("rater_id IN (#{rater_facets_string})").where("dateclosed IS NOT NULL")
		output = ratings.map { |rating| (rating.ratee.master ? rating.ratee.master : rating.ratee) }.uniq
		puts "TRUSTEES:", output
		return output
	end

	def trust_for_user(user)
		if master
			puts "WHY ARE YOU QUERYING TRUST FROM A SLAVE?"
		end

		rater_facets = []
		if master or facets
			the_master = (master ? master : self)
			rater_facets << the_master.id
			the_master.facets.each do |facet|
				rater_facets << facet.id
			end
		else
			rater_facets << id
		end
		
		#Test 1
		if rater_facets.include? id
			puts "GOOD!1"
		else
			raise "rater_facets did not contain my own id!"
		end
		
		ratee_facets = []
		if user.master or user.facets
			if user.master
				puts "WHY ARE YOU QUERYING TRUST TO A SLAVE?"
			end
			the_master = (user.master ? user.master : user)
			ratee_facets << the_master.id
			the_master.facets.each do |facet|
				ratee_facets << facet.id
			end
		else
			ratee_facets << user.id
		end
		
		
		#Test 2
		if rater_facets.include? id
			puts "GOOD!2"
		else
			raise "ratee_facets did not contain target user's own id!"
		end
		rater_facets_string = rater_facets.join(",")
		ratee_facets_string = ratee_facets.join(",")
		ratings = Rating.where("rater_id IN (#{rater_facets_string}) AND ratee_id IN (#{ratee_facets_string}) AND dateclosed IS NOT NULL")
		puts "List of ratings notionally from "+username+" to "+user.username
		puts ratings
		ratings.map { |rating| rating.value }.inject(:+) || 0
	end
	
	def self.random_5_characters
		output = '     '
		for i in 0..4 do
			output[i] = '0123456789abcdefghijklmnopqrstuvwxyz'[SecureRandom::random_number(36)]
		end
		return output
	end

	def subsidiaries 
		User.where('master_id = ?', self.master_id) 
	end
	
	def self.get_unique_username(desired, referrer)
		name = desired.gsub(/[^0-9a-z]/i, '').downcase
		if name == ''
			name = random_5_characters
		end
		name = referrer + "_" + name
		if name.length > 30
			name = name[0..29]
		end
		index = 2
		base_name = name
		while !(User.find_by_username(name).nil?) do #as long as the username is still taken,
			index_string = index.to_s
			cutoff = [30-index_string.length, base_name.length].min
			name = base_name[0..cutoff-1] + index_string
			index += 1
		end #Trust that this will eventually terminate...
		return name
	end

	def awaiting
	  return Rating.where("rater_id = ?", id).where("dateclosed IS NULL").count
	end



end
