class Rating < ActiveRecord::Base
  belongs_to :rater, :class_name => 'User'
  belongs_to :ratee, :class_name => 'User'
  belongs_to :referrer

  attr_accessible :rater, :ratee, :dateopened, :dateclosed, :unlimited, :lowerlimit, :upperlimit, :value, :comment, :unique_token
 

	
  validates_inclusion_of :unlimited, :in => [true, false]
  validates :value, :numericality => { :only_integer => true }
  validate :rating_valid, :on => :update
  validate :limits_valid

  	def is_in_range(proposed_value)
  		if unlimited
  			return true
  		else
  			if proposed_value < -lowerlimit or proposed_value > upperlimit
  				return false
  			else
  				return true
  			end
  		end
  	end

	def limits_valid
		if !unlimited
			unless lowerlimit.class == Fixnum
				errors.add(:lowerlimit, "has to be an integer")
			end

			unless upperlimit.class == Fixnum
				errors.add(:upperlimit, "has to be an integer")
			end

			unless lowerlimit.to_i > 0
				errors.add(:lowerlimit, "has to be greater than zero")
			end

			unless upperlimit.to_i > 0
				errors.add(:upperlimit, "has to be greater than zero")
			end
		end
	end

	def rating_valid
		if !value.nil? and !unlimited
			if value < -lowerlimit
				errors.add(:value, "is too low")
			elsif value > upperlimit
				errors.add(:value, "is too high")
			end
		end
	end


	def color
		return color_for_score(value)
	end
	
	def invitation_dict
		{:inviter => ratee.pretty_name,
		:invitee => rater.pretty_name,
		:upper_limit => unlimited ? 0 : upperlimit,
		:lower_limit => unlimited ? 0 : -1*lowerlimit,
		:date_opened => dateopened}
	end
	
	def set_master(master_user)
		master = master_user
		save
	end
	
	def signed_value
		if value == 0
			return '0'
		elsif value > 0
			return '+'+value.to_s
		elsif value < 0
			return '&minus;'+(-1*value).to_s
		end
	end
	
	#def can_only_rate_once
	#	errors.add(:value, "can only be rated once") if !dateclosed.nil?
	#end
end
