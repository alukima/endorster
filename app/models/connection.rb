require 'securerandom'

class Connection < ActiveRecord::Base
	belongs_to :user

	validates_presence_of :website_name
	validates_presence_of :homepage_url
	#validate :website_url_is_valid
	validate :homepage_url_is_valid

	#def website_url_is_valid
	#	if website_url.include? " "
	#		errors.add(:website_url, "includes invalid characters")
	#	end
	#end

	def homepage_url_is_valid
		if homepage_url.include? " "
			errors.add(:homepage_url, "includes invalid characters")
		end
	end

	def self.create_hash
		SecureRandom::base64(12).gsub('+','-').gsub('/','_')
	end
	
	def short_description
		if website_name.length > 22
			website_name[0..19] + "..."
		else
			website_name
		end
	end

	def show_homepage_url
		sanitize_link homepage_url
	end

	def show_website_url
		sanitize_link website_url
	end

	#private #What is this doing here?

	def sanitize_link(url)
		prefix = "http://"
		url = prefix + url unless url.include? prefix unless url == ""
		return url
	end
end
