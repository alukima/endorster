require 'digest'
require 'application_helper'
require 'openssl'

include ApplicationHelper


#database fields:
#name: string
#title: string
#url: string
#created_date: date
#secure_authentication: boolean
#api_key: string
#public_key: string
#next_sequence: integer


class Referrer < ActiveRecord::Base

	#attr_accessor :public_key

	def authenticate(secret)
		return (!secure_authentication and (Digest::SHA256::hexdigest(secret) == hashed_key))
	end
	
	def self.create_new(referrer_name, public_key=nil)
		if Referrer.find_by_name(referrer_name).nil?
			ref = Referrer.new
			ref.name = referrer_name
			output = true
			if public_key.nil?
				secret = ApplicationHelper.random_token
				ref.hashed_key = Digest::SHA256::hexdigest(secret)
			else
				begin
					k = OpenSSL::PKey.read(ref.public_key)
					ref.public_key = public_key
				rescue
					puts "public_key is not valid"
					return nil
				end
			end
			if ref.save
				return secret
			else
				puts "Could not create referrer, for some reason"
				return nil
			end
		else
			puts "A referrer already exists by that name."
			return nil
		end
	end

end
