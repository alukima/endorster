class HipsterRating < ActiveRecord::Base
  belongs_to :rater, :class_name => 'User'
  belongs_to :ratee, :class_name => 'User'
  belongs_to :intermediary, :class_name => 'User'
  
  attr_accessible :rater, :ratee, :intermediary, :dateopened, :dateclosed, :weight, :value, :comment
 
	validate :rater_doesnt_exist
	#validate :can_only_rate_once
	validates	:ratee, :presence => true
	validates	:intermediary, :presence => true

  validates :weight, :numericality => { :only_integer => true, :greater_than => 0 }
  validates :value, :numericality => { :only_integer => true }
  validate :abs_value_corresponds_with_weight, :on => :update

	def abs_value_corresponds_with_weight
		errors.add(:value, "is of too large magnitude") unless !value || value.abs <= weight
	end

	def rater_doesnt_exist
		errors.add(:rater, "doesn't exist") if rater.nil?
	end

	def color
		red = "ff"
		green = "ff"
		prob = 1/(1+Math.exp(-value/100.0))
		if prob < 0.5
			green = (510*prob).round.to_s(16)
			if green.length == 1
				green = "0"+green
			end
		elsif prob > 0.5
			red = (510*(1-prob)).round.to_s(16)
			if red.length == 1
				red = "0"+red
			end
		end
		blue = "00"
		red+green+blue
	end
	
	#def can_only_rate_once
	#	errors.add(:value, "can only be rated once") if !dateclosed.nil?
	#end
end
