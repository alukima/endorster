class Notification < ActiveRecord::Base
	belongs_to :user
	def full_url
		base_url + thing_id
	end
	
	def self.generate_notification(symbol, record)
		notification = self.new
		if symbol == :got_invitation
			notification.user = record.rater
			notification.text = record.ratee.pretty_name + " has invited you to rate them."
			notification.base_url = "/rate/"
			notification.thing_id = record.id.to_s
		elsif symbol == :invitation_accepted
			notification.user = record.ratee
			notification.text = record.rater.pretty_name + " has accepted your rating invitation."
			notification.base_url = "/invitation/"
			notification.thing_id = record.id.to_s
		elsif symbol == :got_rated
			notification.user = record.ratee
			notification.text = record.rater.pretty_name + " has rated you with value " + record.signed_value + "."
			notification.base_url = "/home/"
			notification.thing_id = ""
		elsif symbol == :identity_claim
			notification.user = record.to
			notification.text = record.from.pretty_name + " claims to be the same person as you. Is this true?"
			notification.base_url = "/profile/connect/"
			notification.thing_id = record.id.to_s
		else
			raise 
		end
		return notification
	end
end
