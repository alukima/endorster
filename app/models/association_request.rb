class AssociationRequest < ActiveRecord::Base

	validates_presence_of :to
	validates_presence_of :from

  belongs_to :from, :class_name => 'User'
  belongs_to :to, :class_name => 'User'
  
  attr_accessible :from, :to, :is_from_master_to_facet
  
end
