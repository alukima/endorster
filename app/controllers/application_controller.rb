class ApplicationController < ActionController::Base
  protect_from_forgery
	rescue_from ActiveRecord::RecordNotFound, :with => :not_found
  
  helper_method :current_user

  
	def not_found(exception = nil)
		puts "@@@@@@@@@@@ RECORD NOT FOUND! #{exception.inspect}"
	end
  
	def login_required
		redirect_to "/" unless current_user
	end

  private
  
  def current_user_session
    return @current_user_session if defined?(@current_user_session)
    @current_user_session = UserSession.find
  end
  
  def current_user
    return @current_user if defined?(@current_user)
    @current_user = current_user_session && current_user_session.record
  end


end

