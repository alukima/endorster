class UserSessionsController < ApplicationController
  def new
    @user_session = UserSession.new
    if params[:username]
    	@user_session.username = params[:username]
    end
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @user_session }
    end
  end

  def create
    @user_session = UserSession.new(params[:user_session])
    next_url = params[:next]
    respond_to do |format|
      if @user_session.save
    		if next_url.nil?
    			format.html { redirect_to '/home' }
    		else
    			format.html { redirect_to next_url }
    		end
        #format.json { render json: @user_session, status: :created, location: @user_session }
      else
        format.html { render action: "new" }
        #format.json { render json: @user_session.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @user_session = UserSession.find
    @user_session.destroy

    respond_to do |format|
      format.html { redirect_to '/home' }
      format.json { head :no_content }
    end
  end
end
