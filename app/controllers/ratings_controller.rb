require 'digest'
require 'application_helper'
include ApplicationHelper

class RatingsController < ApplicationController


	def new
		bubbles
	  @rating = Rating.new
		respond_to do |format|
			format.html  # new.html.erb
			format.json  { render :json => @rating }
		end
	end

	

	def create
		unlimited = params[:rating]["unlimited"]
		if unlimited != '0'
			upp = 1
			low = 1
		else
			upp = params[:rating]["upperlimit"]
			low = params[:rating]["lowerlimit"]
		end
		rater_user = User.find_by_username(params[:rating]["rater"].downcase)
		@rating = Rating.new(
			:ratee => current_user,
			:rater => rater_user,
			:unlimited => params[:rating]["unlimited"],
			:upperlimit => upp,
			:lowerlimit => low,
			:comment => params[:rating]["comment"],
			:dateopened => DateTime.now,
			:value => 0)
		user_found = true
	 	if rater_user.nil? and params[:rating]["rater"] != ''
			@rating.errors[:base] << "Can't find user"
			user_found = false
		end
		respond_to do |format|
			if @rating.save and user_found
			  format.html  { redirect_to(@rating.rater,
							:notice => 'Invitation was sent successfully.') }
			  format.json  { render :json => @rating,
							:status => :created, :location => @rating}
			else
			  format.html  { render :action => "new" }
			  format.json  { render :json => @rating.errors,
							:status => :unprocessable_entity }
			end
		end
	end

	

	#def show #This should never be called.
	#	redirect_to root_url
	#end
	


	def rate
		@rating = Rating.find(params[:id])
		
		if @rating.dateclosed or current_user.nil? or @rating.rater != current_user
			redirect_to root_url
		else
			bubbles
			respond_to do |format|
				format.html  # show.html.erb
				format.json  { render :json => @post }
			end
		end
	end



	def update
	  @rating = Rating.find(params[:id])
		if @rating.rater == current_user and @rating.dateclosed == nil
			@rating.review = params[:rating][:review]
			@rating.value = params[:rating][:value]
			@rating.dateclosed = DateTime.now
			saved = @rating.save
		else
			redirect_to root_url
		end
		if saved
			Notification.generate_notification(:got_rated, @rating).save
			redirect_to("/rate", :notice => 'Rating has been confirmed.')
		else
			render :action => "rate", :status => :unprocessable_entity 
		end
	end


	def api_invitation
		error_code = 0
		id = params[:invitation_id]
		if id.nil?
			error_code = 1
			error_message = "Missing required argument: invitation_id"
		else
			@rating = Rating.find_by_id(id)
			if @rating.nil? or !@rating.dateclosed.nil?
				error_code = 4
				error_message = "invitation_id is invalid, or refers to a nonexistent or closed invitation"
			end
		end
		respond_to do |format|
			if error_code == 0
				format.json  { render :json => {
					:status=>"success",
					:invitation=>@rating.invitation_dict
				} }
			else	
				format.json  { render :json => {
					:status=>"error",
					:error_code=>error_code,
					:error_message=>error_message
				} }
			end
		end
	end

	

	def accept_invitation
		@rating = Rating.find_by_unique_token(Digest::SHA512::hexdigest(params[:id]))
		@rating.rater = User.find_by_username(params[:rating][:rater].downcase)
		@rating.unique_token = nil
		@rating.dateopened = DateTime.now

		# Around this point, we want to notify the issuer of the invitation that their invitation has been accepted.
		if @rating.save
			@notification = Notification.generate_notification(:invitation_accepted, @rating)
			@notification.save
			redirect_to("/rate", :notice => 'You have attached the invitation to your account.')
		else
			render :action => "accept_invitation", :status => :unprocessable_entity 
		end
	end
	
	
	def api_rate
		required_arguments = ['referrer','api_key','invitation_id','privilege_token','value']
		missing_arguments = ApplicationHelper.missing_arguments(params, required_arguments)
		if missing_arguments
			error_code = 1
			error_message = "Missing required arguments: " + missing_arguments
		else
			referrer = params[:referrer]
			api_key = params[:api_key]
			invitation_id = params[:invitation_id].to_i
			privilege_token = params[:privilege_token]
			value = params[:value].to_i

			
			
			ref = Referrer.find_by_name(referrer)
			invitation = Rating.find_by_id(invitation_id)
			if !invitation.dateclosed.nil?
				invitation = nil
			end
			
			if ref.nil? or !ref.authenticate(api_key)
				error_code = 3
				error_message = "referrer + api_key combination is invalid"
			elsif invitation.nil?
				error_code = 4
				error_message = "invitation_id is invalid or do not exist"
			elsif invitation.rater.privilege_tokens[referrer] != Digest::SHA512::hexdigest(privilege_token)
				error_code = 6
				error_message = "referrer lacks sufficient privileges to perform the requested action"
			elsif !invitation.unlimited and (value<(-invitation.lowerlimit) or value>invitation.upperlimit)
				error_code = 5
				error_message = "value is out of range: must be between lower_limit and upper_limit inclusive"
			else
				#Proceed!
				invitation.value = value
				invitation.dateclosed = DateTime.now
				if invitation.save and Notification.generate_notification(:got_rated, invitation).save
					error_code = 0
				else
					error_code = 9
					error_message = "Internal server error"
				end
			end
		end
		

		if error_code == 0
			@output = {:status=>"success"}
		else
			@output = {:status=>"error", :error_code=>error_code, :error_message=>error_message}
		end
		
		respond_to do |format|
			format.json { render json: @output}
		end
	end

end



