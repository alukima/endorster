class WelcomeController < ApplicationController
  def index
	if !current_user.nil?
		redirect_to(current_user)
	end
  end
  
  def login
  end
  
  def about
  end
  
  def help
  end
  
  def api_docs
  end
  
  def legal
  end
  
  def logout
  end
  
  def open
  end
  
  def rate
  end
  
  def user
  end
end
