class ConnectionsController < ApplicationController
	before_filter :login_required, :only => [:index, :new, :edit]
	include ApplicationHelper

	def index
		bubbles
		@connections = Connection.where("user_id = ?", current_user.id).reverse
		@facets = current_user.facets
		respond_to do |format|
		  format.html # index.html.erb
		  format.json { render json: @connections }
		end
	end

	def show
		@connection = Connection.find_by_unique_token(params[:token])
		if !@connection.nil?
			if @connection.user.username != params[:username].downcase
				@connection = nil
			end
		end

		respond_to do |format|
			format.html
			format.json { render json: @connection }
		end
	end

	def new
		bubbles
		@connection = Connection.new

		respond_to do |format|
		  format.html # new.html.erb
		  format.json { render json: @connection }
		end
	end

	def edit
		@connection = Connection.find_by_unique_token(params[:token])
		if @connection.user != current_user
			redirect_to("/")
		else
			if !@connection.nil?
				#target_url = '/connections/'+@connection.user.pretty_name+'/'+@connection.unique_token+'/update/'
				if @connection.user.username != params[:username].downcase
					@connection = nil
				end
			end
			respond_to do |format|
				format.html # edit.html.erb
				format.json { render json: @connection }
			end
		end
	end

	def create
		@connection = Connection.new(params[:connection])
		@connection.user = current_user
		@connection.unique_token = Connection.create_hash
		respond_to do |format|
			if @connection.save
				target_url = '/profile'# '/connections/'+@connection.user.pretty_name+'/'+@connection.unique_token
				format.html { redirect_to target_url, notice: 'Connection was successfully created.' }
				format.json { render json: target_url, status: :created, location: @connection }
			else
				format.html { render action: "new" }
				format.json { render json: @connection.errors, status: :unprocessable_entity }
			end
		end
	end

	def update
		@connection = Connection.find_by_unique_token(params[:token])
		if !@connection.nil?
			if @connection.user.username != params[:username].downcase
				@connection = nil
			end
		end
		respond_to do |format|
			if @connection.update_attributes(params[:connection])
				format.html { redirect_to '/connections/', notice: 'Connection was successfully updated.' }
				format.json { head :no_content }
			else
				format.html { render action: "edit" }
				format.json { render json: @connection.errors, status: :unprocessable_entity }
			end
		end
	end

	def destroy
		@connection = Connection.find(params[:id])
		if @connection.user != current_user
			redirect_to "/"
		else
			@connection.destroy
			respond_to do |format|
				format.html { redirect_to connections_url }
				format.json { head :no_content }
			end
		end
	end
end
