require 'depth_first_search'
require 'digest'
require 'application_helper'
include ApplicationHelper

class UsersController < ApplicationController
	before_filter :login_required, :only => [:show_settings, :confirm_ratings, :associate, :request_association]
	before_filter :bubbles
	def index
		@users = User.all
		
		respond_to do |format|
			format.html # index.html.erb
			#format.json { render json: @users } #must be disabled for security reasons
		end
	end
	
	def associate
		@association_request = AssociationRequest.new
		respond_to do |format|
			format.html # associate.html.erb
		end
	end
	

		# format.html { redirect_to '/profile', notice: 'updated profile text' }
		# 	else
		# 		format.html { render action: "connections#index" }

		def request_association
			request = {
				:from => current_user,
				:to => User.find_by_username(params[:association_request][:to].downcase),
				:is_from_master_to_facet => false 
			}
			@association_request = AssociationRequest.new(request)
			if request[:to].nil?
				@notice = 'The name entered is not a valid Endorster username.'
			elsif !request[:is_from_master_to_facet] and !current_user.master.nil?
			#Your account is already owned by user x, so it can't be owned by a different user as well.
			@notice = "Your account is already owned by user "+current_user.master.pretty_name+", so it can't be owned by a different user as well."
		end
		respond_to do |format|
			if @association_request.errors.empty? and @association_request.save
				Notification.generate_notification(:identity_claim, @association_request).save
				@notice = 'Your connection request has been sent. Log in as '+request[:to].pretty_name+' to accept it.'
				format.html { redirect_to '/settings', notice: @notice }
			else
				#format.json { render json: @user.errors, status: :unprocessable_entity }
				format.html { redirect_to '/settings', notice: @notice  }
			end
		end
	end
	

	def answer_association
		@association_request = AssociationRequest.find_by_id(params[:id])
		if @association_request.nil? or @association_request.to != current_user
			respond_to do |format|
				format.html { redirect_to '/' }
			end
		else
			respond_to do |format|
				format.html
			end
		end
	end

	def reject_association
		ar = AssociationRequest.find_by_id(params[:id])
		if ar.to != current_user
			respond_to do |format|
				format.html { redirect_to '/' }
			end
		else
			ar.delete
			dismiss_notifications
			respond_to do |format|
				format.html { redirect_to '/profile', notice: 'The connection has been rejected.' }
			end
		end
	end

	def confirm_association #Merge the two accounts. Cannot be undone! (Or shouldn't, at least.)
		ar = AssociationRequest.find_by_id(params[:id])
		if ar.to != current_user
			respond_to do |format|
				format.html { redirect_to '/' }
			end
		else
			ar.from.master = current_user
			ar.from.facets.each do |facet|
				facet.master = current_user
				facet.save
			end
			ar.from.save
			
			#If there are any ratings which are revealed to be self-ratings, then mark them as such.
			person_ids = [current_user.id]
			current_user.facets.each do |facet|
				person_ids << facet.id
			end
			person_ids_string = person_ids.join(",")
			self_ratings = Rating.where("rater_id IN (#{person_ids_string}) AND ratee_id IN (#{person_ids_string})")
			#self_ratings.each do |sr|
			#	sr.is_self_rating = true #needs to be implemented
			#	sr.save
			#end
			
			dismiss_notifications
			respond_to do |format|
				format.html { redirect_to '/profile', notice: 'The connection has been confirmed.' }
			end
		end
	end

	def dynamic_score
		user = User.find_by_username(params[:username])
		if user
			@score = DepthFirstSearch.score(current_user, user) unless current_user == @user || current_user.nil?
			@backscore = DepthFirstSearch.score(user, current_user) unless current_user == @user || current_user.nil?
			bs_color = color_for_score(@backscore)
			s_color = color_for_score(@score)
		else
			@error = "Couldn't find that user"
		end
		respond_to do |format|
			msg = { score: @score, backscore: @backscore, backscore_color: bs_color, score_color: s_color, error: @error }
			format.json  { render :json => msg } 
		end
	end

	def dynamic_search
		term = params[:term]
		users = User.all
		values = []
		users.each do |u|
			values << u.username if u.username.include?(term)
		end
		puts values


		respond_to do |format|
			format.json  { render :json => values }
		end
	end

	def show
		@page = 'overview'
		@viewing_user = User.find_by_username(params[:id].downcase)
		@facets = @viewing_user.facets
		@score = DepthFirstSearch.score(current_user, @viewing_user) unless current_user == @viewing_user || current_user.nil?
		@backscore = DepthFirstSearch.score(@viewing_user, current_user) unless current_user == @viewing_user || current_user.nil?
		if @viewing_user == current_user
			@association_requests = AssociationRequest.where("to_id =?", current_user)
		else
			@association_requests = []
		end
		@connections = Connection.where("user_id = ?", @viewing_user).reverse
		respond_to do |format|
			if @viewing_user
				@iratings = Rating.where("ratee_id = ?", @viewing_user.id).where("dateclosed IS NOT NULL")
				@oratings = Rating.where("rater_id = ?", @viewing_user.id).where("dateclosed IS NOT NULL")
	        format.html # show.html.erb
	      else
	      	format.html { redirect_to users_url, :notice => "User doesn't exist" }
	      end
	    end
	  end

	  def notifications
	  	@user = User.find_by_username(params[:username])
	  	@notifications = @user.notifications
	  end

	  def activity
	  	@page = 'activity'
	  	@viewing_user = User.find_by_username(params[:username].downcase)
	  	@ratings =  @viewing_user.outgoingratings + @viewing_user.incomingratings 
	  	@ratings.sort { |x,y| y.created_at <=> x.created_at}
	  end

	  def dismiss_notification
	  	if current_user
	  		@current_user.notifications.each do |notification|
	  			notification.destroy
	  		end
	  	else
			# do nothing.
		end
	end

	def mark_read
		notification = Notification.find(params[:id])
		notification.read = true
		notification.save
		if notification.base_url == "/invitation/"
			render :js => "window.location = #{notification.base_url+notification.thing_id}"
		else
			render :js => "window.location = #{notification.base_url}"
		end
	end
	
	def home
		respond_to do |format|
			if current_user
				format.html { redirect_to '/user/'+current_user.pretty_name }
			else
				format.html { redirect_to '/' }
			end
		end
	end
	
	def api_score
		@error_message = ""
		@error_code = 1
		puts params
		if params.nil? or (params[:from].nil? or params[:to].nil?)
			@error_message = "Missing required arguments: " + ApplicationHelper.missing_arguments(params, ['from','to'])
		else
			@error_code = 4
			@user_from = User.find_by_username(params[:from].downcase)
			
			@user_to = User.find_by_username(params[:to].downcase)
			if @user_from.nil? and @user_to.nil?
				@error_message = "'from' and 'to' usernames are invalid or do not exist"
			elsif @user_from.nil?
				@error_message = "'from' username is invalid or does not exist"
			elsif @user_to.nil?
				@error_message = "'to' username is invalid or does not exist"
			else
				@error_code = 0
				if @user_from == @user_to
					@score = nil
				elsif !@user_from.master.nil? and (@user_from.master == @user_to or @user_from.master == @user_to.master)
					@score = nil
				elsif !@user_to.master.nil? and (@user_to.master == @user_from or @user_from.master == @user_to.master)
					@score = nil
				else
					@score = DepthFirstSearch.score(@user_from, @user_to)
				end
			end
		end
		
		
		if @error_code == 0
			@output = {:status=>"success", :score=>@score}
		else
			@output = {:status=>"error",
				:error_code=>@error_code,
				:error_message=>@error_message}
			end

			respond_to do |format|
				format.json { render json: @output}
			end
		end

		def new
			@user = User.new
			respond_to do |format|
			format.html # new.html.erb
			format.json { render json: @user }
		end
	end
	
	
	
	def send_verification_email(address, user)
		@code = ApplicationHelper.random_token
		user.code = @code
		if user.save
			begin
				MyMailer.verificationmail(address, user.pretty_name, @code).deliver
			rescue Exception => e
				puts "EMAIL FAILED TO SEND"
				puts e.message
				return "Verification link could not be sent."
			end
			return "A verification link has been emailed to you."
		else
			return "Verification link could not be sent."
		end
	end


	def verification_email
		if current_user.nil?
			respond_to do |format|
				format.html { redirect_to '/' }
			end
		else
			respond_to do |format|
  				#attempt to send the email.
  				@notice = send_verification_email(current_user.email, current_user)
  				puts @notice
  				puts "EMAIL!"
  				format.html { redirect_to '/user/'+current_user.pretty_name, :notice => @notice }
  			end
  		end
  	end
  	
  	
  	def verify
  		@code = params[:code]
  		@notice = "We couldn't verify your email with this link. Try resending the verification email."
  		if current_user.code == @code
  			current_user.email_verified = true
  			current_user.code == nil
  			if current_user.save
  				@notice = "Email successfully verified!"
  			end
  		end
  		respond_to do |format|
  			format.html { redirect_to '/user/'+current_user.pretty_name, :notice => @notice }
  		end
  	end




  	def show_settings
  		@association_request = AssociationRequest.new
  		@user = current_user
  		respond_to do |format|
			format.html # show_settings.html.erb
		end
	end
	
	def confirm_ratings
		@user = current_user
		@envelopes = Rating.where("rater_id = ?", current_user.id).where("dateclosed IS NULL")
		respond_to do |format|
			format.html # confirm_ratings.html.erb
			#format.json { render json: {:data => {:user => @user, :envelopes => @envelopes}}}
		end
	end
	
	def r
		@user = User.new(params[:user])
		@user.pretty_name = @user.username
		@user.username = @user.pretty_name.downcase
		next_url = params[:next]
		if @user.email == ""
			@msg = ""
		else
			@msg = " "+send_verification_email(@user.email, @user)
		end
		
		respond_to do |format|
			if @user.save
				if next_url.nil?
					format.html { redirect_to '/user/'+@user.pretty_name, notice: 'Welcome to Endorster, '+@user.pretty_name+'!'+@msg }
				else
					format.html { redirect_to next_url, notice: 'Welcome to Endorster, '+@user.pretty_name+'!'+@msg }
				end
				format.json { render json: @user, status: :created, location: @user }
			else
				format.html { render action: "new" }
				format.json { render json: @user.errors, status: :unprocessable_entity }
			end
		end
	end
	
	def check_password(usr, pwd)
		if !usr.inactive and (usr.valid_password? pwd)
			return true
		else
			usr.errors.add(:current_password, 'is invalid')
			return false
		end
	end


	def update_description
		new_description = params[:user][:description]
		current_user.description = new_description.split.join(" ")
		respond_to do |format|
			if current_user.save
				format.html { redirect_to '/profile', notice: 'updated profile text' }
			else
				format.html { render action: "connections#index" }
			end
		end
	end
	
	def update
		@user = current_user
		@new_email = params[:user][:email]
		if @new_email != "" and @user.email != @new_email
			@email_updated = true
		else
			@email_updated = false
		end
		
		respond_to do |format|
			if check_password(@user, params[:user][:current_password]) 
				@user.email = @new_email
				if @email_updated
					@user.email_verified = false
					@msg = " "+send_verification_email(@new_email, @user)
				else
					@msg = ""
				end
				@user.password = params[:user][:password]
				@user.password_confirmation = params[:user][:password_confirmation]
				if @user.save
					format.html { redirect_to '/settings', notice: 'Settings successfully updated.'+@msg }
				else
					@association_request = AssociationRequest.new
					format.html { render action: "show_settings" }
				end
			else
				puts "USER ERRORS2!", @user.errors.full_messages.join("\n")
				@association_request = AssociationRequest.new
				format.html { render action: "show_settings" }
			end
		end
	end
end