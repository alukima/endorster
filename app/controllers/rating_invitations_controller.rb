require 'application_helper'
require 'digest'
include ApplicationHelper


class RatingInvitationsController < ApplicationController
	before_filter :login_required, :only => [:index, :show, :email_invite, :send_email_invite]
  before_filter :bubbles, :only => [:index, :show, :email_invite, :send_email_invite]
	def index
		invites = Rating.where(:ratee_id => current_user.id).where("dateclosed IS NULL")
		@pending_invites = invites.where("rater IS NOT NULL")
		@unaccepted_invites = invites.where("rater IS NULL")
		respond_to do |format|
			format.html # index.html.erb
		end
	end

	def show
		@rating = Rating.find(params[:id])
		if @rating.ratee == current_user
			@unique = false
			render :action => 'show'
		else
			raise ActionController::RoutingError.new('Not Found')
		end
	end
	
	def show_unique
		@secret_token = params[:id]
		@rating = Rating.find_by_unique_token(Digest::SHA512::hexdigest(@secret_token))
		@unique = true
		render :action => 'show'
	end

	def new
		@mass_email = MassEmail.new(params[:mass_email])
		@mass_email.subject ||= "Rate me on Endorster!"
		@mass_email.message ||= """
Hello!

Endorster is a free platform for managing your online reputation. If you know me and you think I'm nice, rate me positively at the link below:

{LINK}

If you rate me now, your own score will increase for each person who rates me well later - so sign up and rate me today! To find out more, visit www.endorster.net/about.

Thank you!\n\n""" + current_user.pretty_name
		@rating = Rating.new(params[:rating])
		@link = Link.new(params[:link])

		invites = Rating.where(:ratee_id => current_user.id).where("dateclosed IS NULL")
		@pending_invites = invites.where("rater_id IS NOT NULL")
		@unaccepted_invites = invites.where("rater_id IS NULL")
		respond_to do |format|
			format.html # new.html.erb
		end
	end

	def email_invite
		if current_user
			@presets = {"lowerlimit"=>1, "upperlimit"=>10, "subject"=>"Invitation to rate me on Endorster", "message"=>"Hello!\n\nEndorster is a free platform for managing your online reputation. If you know me and you think I'm nice, rate me by following the link below:\n\n{LINK}\n\nIf you rate me now, your own score will increase for each person who rates me well later - so sign up and rate me today! To find out more, visit www.endorster.net/about.\n\nThank you!\n"+current_user.pretty_name}
			@mass_email = MassEmail.new(@presets)
			respond_to do |format|
				format.html { render action: "email_invite"}
	
			end
		else
			respond_to do |format|
				format.html {redirect_to('/')}
			end
		end
	end


	def send_email_invite
		@mass_email = MassEmail.new(params[:mass_email])
		@addresses = @mass_email.invitees.split(", ")

		#copied verbatim from Rating model; should be refactored later.
		if !@mass_email.unlimited and
				(@mass_email.lowerlimit.nil? or @mass_email.upperlimit.nil? or
				@mass_email.lowerlimit.class != Fixnum or @mass_email.upperlimit.class != Fixnum or @mass_email.lowerlimit <= 0 or @mass_email.upperlimit <= 0)
			@mass_email.errors.add(:lowerlimit, "must be an integer greater than 0")
			@mass_email.errors.add(:upperlimit, "_")
			@errors = true

		end
		
		if @addresses.length == 0
			@mass_email.errors.add(:invitees,"must contain at least one email address")
			@errors = true
		else
			@addresses.each do |address|
				if !(/\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i =~ address)
					@mass_email.errors.add(:invitees, "List has one or more invalid addresses")
					@errors = true
				end
			end
		end
		


		if !(@mass_email.message.include? "{LINK}")
			@mass_email.errors.add(:message, "must contain the string {LINK} in order to include an invitation link for each invitee.")
			@errors = true
		end
		

		
		respond_to do |format|
			if !@errors
				@successes = 0
				@addresses.each do |address|
					#Create a rating invitation
					@rating = Rating.new
					@rating.ratee = current_user
					@rating.comment = "Email invitation for "+address
					@rating.lowerlimit = @mass_email.lowerlimit
					@rating.upperlimit = @mass_email.upperlimit
					@rating.unlimited = @mass_email.unlimited
					#@rating.dateopened = DateTime.now #The invitation is not "opened" until it's attached.
					@rating.value = 0
					secret_token = random_token
					@rating.unique_token = Digest::SHA512::hexdigest(secret_token)
					@link = "www.endorster.net/i/"+secret_token
					if @rating.save
						@message_with_link = @mass_email.message.gsub("{LINK}",@link)
						begin
							MyMailer. invitationemail(current_user.email, address, @mass_email.subject, @message_with_link).deliver
							@successes += 1
						rescue Exception => e
							@rating.destroy
							puts "EMAIL FAILED TO SEND."
							puts e.message
						end
					else
						puts "Validation failed for some reason"
					end
				end
				if @successes == 0
					@notice = "Emails could not be sent. Try again later, and/or check that the addresses are valid."
				elsif @successes == 1
					@notice = "1 invitation has been sent."
				else
					@notice = @successes.to_s + " invitations have been sent."
				end
				@mass_email.destroy
				puts "IT'S HAPPENING!!!"
				puts @notice
				format.html {redirect_to('/invite', :notice => @notice)}
			else
				puts "ERRORS!"
				puts @errors
				format.html { render action: "email_invite"}
			end
		end
	end


	def create
		@rating = Rating.new
		@rating.rater = User.find_by_username(params[:rating][:rater].downcase)
		@rating.ratee = current_user
		@rating.comment = params[:rating][:comment]
		@rating.lowerlimit = params[:rating][:lowerlimit]
		@rating.upperlimit = params[:rating][:upperlimit]
		@rating.unlimited = params[:rating][:unlimited]
		@rating.value = 0
		
		user_found = true
		if @rating.rater.nil?
			if params[:rating][:rater] == ''
				secret_token = ApplicationHelper.random_token
				@rating.unique_token = Digest::SHA512::hexdigest(secret_token)
			else #Problem: invalid username.
				@rating.errors.add(:rater, 'is not an Endorster user')
				user_found = false
			end
		else
			@rating.dateopened = DateTime.now
			@rating.unique_token = nil
		end
		respond_to do |format|
			if user_found and @rating.save 
				if @rating.rater.nil?
					format.html {redirect_to("/i/"+secret_token, :notice => 'Invitation has been created.')}
				else
					@notification =  Notification.generate_notification(:got_invitation, @rating)
					@notification.save
					format.html {redirect_to("/invite", :notice => 'Invitation has been created.')}
				end
				#format.json {redirect_to "/invite/"}
			else
				@mass_email = MassEmail.new(params[:mass_email])
				@link = Link.new
				invites = Rating.where(:ratee_id => current_user.id).where("dateclosed IS NULL")
				@pending_invites = invites.where("rater_id IS NOT NULL")
				@unaccepted_invites = invites.where("rater_id IS NULL")
				format.html { render action: "new" }
				#render "new"
			end
		end
	end



	
	
	def regenerate
		@rating = Rating.find_by_id(params[:id])
		if @rating.nil? or @rating.ratee != current_user or !@rating.rater.nil?
			raise ActionController::RoutingError.new('Not Found')
		else
			secret_token = ApplicationHelper.random_token
			@rating.unique_token = Digest::SHA512::hexdigest(secret_token)
			@rating.save
			respond_to do |format|
				format.html {redirect_to("/i/"+secret_token, :notice => 'Invitation has been regenerated.')}
			end
		end
	end


	def delete
		@rating = Rating.find_by_id(params[:id])
		if @rating.nil? or !@rating.rater.nil? or current_user.nil? or @rating.ratee != current_user
			raise ActionController::RoutingError.new('Not Found')
		else
			@rating.delete
			respond_to do |format|
				format.html {redirect_to("/invite#outstanding", :notice => 'Invitation has been deleted.')}
			end
		end
	end

end
