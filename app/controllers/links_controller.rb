class LinksController < ApplicationController
	before_filter :login_required, :only => [:index]
  before_filter :bubbles, :only => [:index, :show, :new ]

  def index
		@links = Link.where("user_id = ?", current_user.id).reverse
		@link = Link.new
		respond_to do |format|
		  format.html # index.html.erb
		  format.json { render json: @links }
		end
  end

  def show
    @link = Link.find_by_unique_token(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @link }
    end
  end

  def new
    @link = Link.new
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @link }
    end
  end

  def create
    @link = Link.new(params[:link], current_user)
		@link.user_id = current_user.id
		@link.unique_token = Link.create_hash

    respond_to do |format|
      if @link.save
        format.html { redirect_to '/s/'+@link.unique_token, notice: 'Share was successfully created.' }
        format.json { render json: '/s/'+@link.unique_token, status: :created, location: @link }
      else
        format.html { render action: "new" }
        format.json { render json: @link.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
  	  @link = Link.find_by_unique_token(params[:id])
  	  if current_user.id == @link.user_id
  	  	  @link.destroy
		  respond_to do |format|
			   format.html { redirect_to '/invite#share', notice: 'Sharing link was deleted.' }
		  end
	  else
	  	  respond_to do |format|
			   format.html { redirect_to '/' }
		  end
	  end
  end
end
