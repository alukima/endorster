require 'openssl'
require 'json'

require 'user'
require 'depth_first_search'

require 'rating'


class ApiController < ActionController::Base
  def find_missing_arguments(received_dict, required_arguments)
    output = []
    required_arguments.each do |arg|
      if received_dict[arg].nil?
        output << arg
      end
    end
    return output
  end


  def missing_arguments_message(missing_arguments)
    if missing_arguments.length > 1
      plural = "s"
    else
      plural = ""
    end
    output = ""
    missing_arguments.each do |arg|
      output += ("`"+arg+"`, ")
    end
    output = "Missing required argument" + plural + ": " + output[0..-3]
    return output
  end


  def find_missing_resources(found_dict)
    output = []
    found_dict.keys.each do |key|
      if found_dict[key].nil?
        output << key
      end
    end
    return output
  end


  def missing_resources_message(missing_resources)
    if missing_resources.length > 1
      plural = "s"
      verb = "are"
      article = "a "
      conjugation = ""
    else
      plural = ""
      verb = "is"
      article = ""
      conjugation = "s"
    end
    output = ""
    missing_resources.each do |arg|
      output += ("`"+arg+"`, ")
    end
    output = "Argument"+plural+" "+verb+" invalid or refer"+conjugation+" to "+article+"nonexistent or expired resource"+plural+": "+output[0..-3]
    return output
  end

  VALID_SIGNATURE_FORMATS = ["SHA256_BIG_Base64+/="]
  VALID_ACTIONS = ["signup", "invite", "rate", "initiate_activation"]
  #POST
  def api_secure
    status_code = 0
    begin
      print "SECURE PARAMS:", params
      missing_arguments = find_missing_arguments(params, ["request_body", "referrer", "signature"])
      if missing_arguments.length > 0
        status_code = 400
        error_message = missing_arguments_message(missing_arguments)
      elsif !params[:signature_format].nil? and !(VALID_SIGNATURE_FORMATS.include?(params[:signature_format]))
        status_code = 400
        error_message = "Invalid `signature_format`"
      else
        ref = Referrer.find_by_name(params[:referrer])
        missing_resources = find_missing_resources({"referrer"=>ref})
        if missing_resources.length > 0
          status_code = 401
          error_message = missing_resources_message(missing_resources)
        else
          if !(ref.secure_authentication and !ref.public_key.nil?)
            status_code = 401
            error_message = "`referrer` does not have secure authentication enabled"
          else
            hashed_request_body = OpenSSL::Digest.digest("SHA256",params[:request_body])
            if !params[:hash].nil? and Base64::decode64(params[:hash]) != hashed_request_body
              status_code = 401
              error_message = "`hash` does not match `request_body`"
            else
              crypto_public_key = OpenSSL::PKey.read(ref.public_key) #Load the public key
              begin
                verification = crypto_public_key.sysverify(hashed_request_body, Base64::decode64(params[:signature]))
              rescue
                verification = false
              end
              if !verification
                status_code = 401
                error_message = "`signature` is invalid"
              else
                begin
                  request_body = JSON.load(params[:request_body])
                rescue
                  #JSON could not be parsed.
                  request_body = nil
                end
                if request_body.nil?
                  status_code = 400
                  error_message = "`request_body` is not a valid JSON string"
                else
                  missing_body_arguments = find_missing_arguments(request_body, ["sequence", "action"])
                  if missing_body_arguments.length > 0
                    status_code = 400
                    error_message = "`request_body` is "+missing_arguments_message(missing_body_arguments).downcase
                  elsif !(VALID_ACTIONS.include?(request_body["action"]))
                    status_code = 501
                    error_message = "'" + request_body["action"] + "' is not a valid `action`"
                  elsif request_body["sequence"] != ref.next_sequence
                    status_code = 401
                    error_message = "`sequence` is invalid"
                  else
                    #Credentials have been verified!
                    inner_params = request_body["params"]
                    if inner_params.nil?
                      inner_params = {}
                    end
                    inner_params["referrer"] = ref.name
                    #Dispatch inner_params to appropriate iapi handler
                    success = true
                    begin
                      case request_body["action"]
                      when "signup"
                        result = iapi_signup(inner_params)
                      when "invite"
                        result = iapi_invite(inner_params)
                      when "rate"
                        result = iapi_rate(inner_params)
                      when "initiate_activation"
                        result = iapi_initiate_activation(inner_params)
                      else
                        result = nil
                        status_code = 501
                        error_message = "'" + request_body["action"] + "' is not a valid `action`"
                        success = false
                      end
                      if success
                        ref.next_sequence = ref.next_sequence + 1
                        ref.save
                      end
                    rescue
                      result = nil
                      status_code = 500
                      error_message = "Internal server error; sequence not incremented"
                    end
                  end
                end
              end
            end
          end
        end
      end
    rescue Exception => ex
      print "Exception in api_secure:", ex
      status_code = 500
      error_message = "Internal server error"
    end


    #Finally...

    if status_code != 0
      output = {
        :status_code => status_code,
        :error_message => error_message
      }
    else
      output = {
        :status_code => 200,
        :result => result
      }
    end


    respond_to do |format|
      format.json { render json: output}
    end


    #check for optional: format
    #  "Invalid argument `format`"
    #
    #check that referrer exists:
    #  "referrer does not exist"
    #if hash is provided, check that hash matches request_body
    #  "hash does not match request_body"
    #check signature
    #  "signature is invalid"
    #
    #now look at request_body:
    #
    #check required arguments: sequence, action
    #  "Missing required request_body argument:" whatever
    #check action:
    #  "Invalid action"
    #check sequence:
    #  "invalid sequence"
    #finally, the request has been verified.
    #Increment the sequence number in the database and store it to return.
    #Dispatch to the appropriate action with params (if any) and referrer
    #
    #And return it in a dictionary to json.
  
  end

  #GET
  def api_score
    begin
      output = iapi_score(params)
    rescue Exception=>ex
      print "Exception in api_score:", ex
      output = {
        :status_code => 500,
        :error_message => "Internal server error"
      }
    end
    respond_to do |format|
      format.json { render json: output}
    end
  end

  #GET
  def api_next_sequence
    begin
      output = iapi_next_sequence(params)
    rescue Exception=>ex
      print "Exception in api_next_sequence:", ex
      output = {
        :status_code => 500,
        :error_message => "Internal server error"
      }
    end
    respond_to do |format|
      format.json { render json: output}
    end
  end

  #GET
  def api_invitation
    begin
      output = iapi_invitation(params)
    rescue Exception=>ex
      print "Exception in api_invitation:", ex
      output = {
        :status_code => 500,
        :error_message => "Internal server error"
      }
    end
    respond_to do |format|
      format.json { render json: output}
    end
  end


def basic_authentication(params, iapi_method)
  status_code = 0
  missing_arguments = find_missing_arguments(params, ["referrer","secret"])
  if missing_arguments.length > 0
    status_code = 400
    error_message = missing_arguments_message(missing_arguments)
  else
    ref = Referrer.find_by_name(params[:referrer])
    secret = params[:secret]
    if ref.nil? or !ref.authenticate(secret)
      status_code = 401
      error_message = "`referrer`,`secret` combination is invalid"
    else
      #Authentication successful.
      output = iapi_method.call(params)
    end
  end

  if status_code != 0
    output = {
      :status_code => status_code,
      :error_message => error_message
    }
  end
  return output
end


  #POST
  def api_signup
    begin
      output = basic_authentication(params, method(:iapi_signup))
    rescue Exception=>ex
      print "Exception in api_signup:", ex
      output = {
        :status_code => 500,
        :error_message => "Internal server error"
      }
    end
    respond_to do |format|
      format.json { render json: output}
    end
  end

  #POST
  def api_invite
    begin
      output = basic_authentication(params, method(:iapi_invite))
    rescue Exception=>ex
      print "Exception in api_invite:", ex
      output = {
        :status_code => 500,
        :error_message => "Internal server error"
      }
    end
    respond_to do |format|
      format.json { render json: output}
    end
  end

  #POST
  def api_set_public_key
    begin
      output = basic_authentication(params, method(:iapi_set_public_key))
    rescue Exception=>ex
      print "Exception in api_set_public_key:", ex
      output = {
        :status_code => 500,
        :error_message => "Internal server error"
      }
    end
    respond_to do |format|
      format.json { render json: output}
    end
  end

  #POST
  def api_rate
    begin
      output = basic_authentication(params, method(:iapi_rate))
    rescue Exception=>ex
      print "Exception in api_rate:", ex
      output = {
        :status_code => 500,
        :error_message => "Internal server error"
      }
    end
    respond_to do |format|
      format.json { render json: output}
    end
  end

  #Internal processors: First, check the validity of params. Then,r
  #return a dictionary of the sort that the API would return,
  #but don't serve it. (iapi_something)

  #GET
  def iapi_score(params)
    status_code = 0
    missing_arguments = find_missing_arguments(params, ["from","to"])
    if missing_arguments.length > 0
      status_code = 400
      error_message = missing_arguments_message(missing_arguments)
    else
      user_from = User.find_by_username(params[:from].downcase)
      user_to = User.find_by_username(params[:to].downcase)
      missing_resources = find_missing_resources({"from"=>user_from, "to"=>user_to})
      if missing_resources.length > 0
        status_code = 404
        error_message = missing_resources_message(missing_resources)
      else
        #Validation complete
        score = DepthFirstSearch.score(user_from, user_to)
      end
    end

    if status_code == 0
      output = {
        :status_code => 200,
        :result => score
      }
    else
      output = {
        :status_code => status_code,
        :error_message => error_message
      }
    end
    return output

  end


  #GET
  def iapi_next_sequence(params)
    status_code = 0
    missing_arguments = find_missing_arguments(params, ["referrer"])
    if missing_arguments.length > 0
      status_code = 400
      error_message = missing_arguments_message(missing_arguments)
    else
      ref = Referrer.find_by_name(params["referrer"])
      if ref.nil?
        status_code = 404
        error_message = "`referrer` does not exist"
      end
    end

    if status_code == 0
      output = {
        :status_code => 200,
        :result => ref.next_sequence
      }
    else
      output = {
        :status_code => status_code,
        :error_message => error_message
      }
    end
    return output

  end



  #POST
  #Assume that authentication has already succeeded.
  def iapi_signup(params)
    #There are no required arguments.
    status_code = 0
    if params["username"]
      username = params["username"]
    else
      username = ''
    end
    ref_name = params["referrer"]     
    password = params["password"]
    ref = Referrer.find_by_name(ref_name)
    endorster_username = User::get_unique_username(username, ref_name)
    @user = User.new
    @user.username = endorster_username
    @user.pretty_name = endorster_username.downcase
    @user.authorized_referrers = {ref_name => true}
    if password.nil?
      @user.inactive = true
      @user.password = "!!!!"
      @user.password_confirmation = "!!!!"
    else
      @user.inactive = false
      @user.password = password
      @user.password_confirmation = password
    end

    if !@user.save
      status_code=500
      error_message="Internal server error"
    end
    if status_code == 0
      output = {
        :status_code=>200, 
        :result => endorster_username
      }
    else
      output = {
        :status_code=>status_code,
        :error_message=>error_message
      }
    end
    return output
  end

  #Helper method: Check if the provided referrer has access privileges for the account
  def check_privilege(params, user_argument_name)
    status_code = 0
    missing = find_missing_arguments(params, [user_argument_name])
    if missing.length > 0
      status_code = 400
      error_message = missing_arguments_message(missing)
    else
      user = User.find_by_username(params[user_argument_name])
      if user.nil?
        status_code = 404
        error_message = "`"+user_argument_name+"` does not exist"
      else
        referrer = Referrer.find_by_name(params["referrer"])
        if referrer.nil?
          status_code = 500
          error_message = "Internal error: `referrer` not found"
        else
          if !user.authorized_referrers[params["referrer"]]
            status_code = 403
            error_message = "`referrer` lacks privileged access"
          else
            status_code = 0
            error_message = ""
          end
        end
      end
    end
    return {:status_code=>status_code, :error_message=>error_message, :user=>user}
  end






  def iapi_initiate_activation
    privilege_check = check_privilege(params, "username")
    status_code = privilege_check[:status_code]
    error_message = privilege_check[:error_message]
    authorizing_user = privilege_check[:user]
    if status_code == 0
      missing_arguments = find_missing_arguments(params, ["hashed_activation_code"])
      if missing_arguments.length > 0
        status_code = 400
        error_message = missing_arguments_message(missing_arguments)
      else
        authorizing_user.hashed_activation_code = params["hashed_activation_code"]
        if !authorizing_user.save
          status_code = 500
          error_message = "Internal server error"
        end
      end
    end

    if status_code != 0
      output = {
        :status_code => status_code,
        :error_message => error_message
      }
    else
      output = {
        :status_code => 200,
        :result => true
      }
    end
    return output

  end

  #POST
  #Assume that authentication has already succeeded
  def iapi_invite(params)
    status_code = 0
    missing_arguments = find_missing_arguments(params, ['inviter','invitee'])
    if missing_arguments.length > 0
      status_code = 400
      error_message = missing_arguments_message(missing_arguments)
    else
      ref = Referrer.find_by_name(params["referrer"])
      privilege_check = check_privilege(params, "inviter")
      status_code = privilege_check[:status_code]
      error_message = privilege_check[:error_message]
      authorizing_user = privilege_check[:user]
      if status_code == 0
        inviter = authorizing_user
        invitee = User.find_by_username(params["invitee"].downcase)
        upper_limit = params["upper_limit"].to_i
        lower_limit = params["lower_limit"].to_i
        comment = params[:comment]

        if (lower_limit>=0 or upper_limit<=0) and (lower_limit!=0 or upper_limit!=0)
          status_code = 400
          error_message = "`lower_limit` and `upper_limit` must be negative or positive respectively, or else both 0"
        else
          @rating = Rating.new
          @rating.rater = invitee
          @rating.ratee = inviter
          @rating.lowerlimit = -lower_limit #This may change in the future.
          @rating.upperlimit = upper_limit
          @rating.unlimited = (upper_limit==0 and lower_limit==0)
          @rating.dateopened = DateTime.now
          @rating.dateclosed = nil
          @rating.value = 0
          @rating.comment = comment
          @rating.unique_token = nil #Because this is not a free invitation.
          @rating.referrer = ref
          if !@rating.save
            status_code = 500
            error_message = "Internal server error"
          end
        end

      end
    end

    if status_code == 0
      output = {
        :status_code=>200, 
        :result => @rating.id
      }
    else
      output = {
        :status_code=>status_code,
        :error_message=>error_message
      }
    end
    return output
  end


  #POST
  #Assume that authentication has already succeeded
  def iapi_set_public_key(params)
    status_code = 0
    missing_arguments = find_missing_arguments(params, ['public_key'])
    if missing_arguments.length > 0
      status_code = 400
      error_message = missing_arguments_message(missing_arguments)
    else
      ref = Referrer.find_by_name(params["referrer"])
      begin
        k = OpenSSL::PKey.read(params['public_key'])
      rescue
        status_code = 400
        error_message = "`public_key` is not a valid PEM-formatted key"
      else
        ref.public_key = params['public_key']
        ref.secure_authentication = true
        ref.next_sequence = 0
        if !ref.save
          status_code = 500
          error_message = "Internal server error"
        end
      end
    end

    if status_code == 0
      output = {
        :status_code=>200, 
        :result => true
      }
    else
      output = {
        :status_code=>status_code,
        :error_message=>error_message
      }
    end
    return output
  end

  #GET
  def iapi_invitation(params)
    #Find the requested resource
    status_code = 0
    missing = find_missing_arguments(params, ["invitation_id"])
    if missing.length > 0
      status_code = 400
      error_message = missing_arguments_message(missing)
    else
      @rating = Rating.find_by_id(params[:invitation_id])
      if @rating.nil? or !@rating.dateclosed.nil?
        status_code = 404
        error_message = "`id` is invalid, or refers to a nonexistent or expired invitation"
      end
    end

    #Return it if we found it, or an error if we didn't.

    if status_code != 0
      output = {
        :status_code => status_code,
        :error_message => error_message
      }
    else
      output = {
        :status_code => 200,
        :result => @rating.invitation_dict
      }
    end
    return output
  end

  #POST
  #Assume that authentication has already succeeded
  def iapi_rate(params)
    status_code = 0
    missing_arguments = find_missing_arguments(params, ['invitation_id','value'])
    if missing_arguments.length > 0
      status_code = 400
      error_message = missing_arguments_message(missing_arguments)
    else
      rating = Rating.find_by_id(params["invitation_id"])
      if rating.nil? or !rating.dateclosed.nil?
        status_code = 404
        error_message = "`invitation_id` refers to a nonexistent or expired invitation"
      else
        privilege_check = check_privilege({"referrer" => params["referrer"], "the_invitee" => rating.rater.username}, "the_invitee")
        status_code = privilege_check[:status_code]
        error_message = privilege_check[:error_message]
      end
      if status_code == 0
        value = params["value"].to_i
        if !rating.is_in_range(value)
          status_code = 400
          error_message = "`value` is out of range"
        else
          rating.value = value
          rating.dateclosed = DateTime.now
          if !rating.save
            status_code = 500
            error_message = "Internal server error"
          end
        end
      end
    end

    if status_code == 0
      output = {
        :status_code=>200, 
        :result => true
      }
    else
      output = {
        :status_code=>status_code,
        :error_message=>error_message
      }
    end
    return output
  end

end


awegawgag="""
response format:

status: success / error
status_code: 130
error_message: 'something', or
result: {
  status: success / error
  status_code: 7
  error_message: 'something', or
  result: {
    invitation_id: 838
  }
}



Hash or signature failure: 401 Unauthorized
  Invalid hash
  Invalid signature
  Invalid sequence
"""