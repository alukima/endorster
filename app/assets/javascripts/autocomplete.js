

jQuery.fn.searchComplete = function() {
  var o = $(this[0]);
  findTrigger = function(e){
    if (e.keyCode == 38 || e.keyCode == 40 ){
      calculateSelection(e.keyCode);
    } else {
      $('.search').remove()
      sendSearch();
    }
  }

  sendSearch = function(){
    var input = $(o).val();
    selected = 0
    if(input!='')
      $.ajax({
        type: "POST",
        url: "/dynamic_search",
        data: { term: input },
        success: listMatches
      })
  }

  listMatches = function(terms){
    var termsLi = createList(terms)
    list = "<ul class='search'>" + termsLi +"</ul>";
    $(list).appendTo('.test-div');
  }



  createList = function(terms){
    li = ''
    for (var i = terms.length - 1; i >= 0; i--)
      li += "<li style='width: "+ $("#username1").width()+"px;'>" + terms[i]; + "</li>"
    return li
  }

  calculateSelection = function(e){
    if (e == 40){
      selected += 1
      hightlightSelection();
    }else if (e == 38 ) {
      selected -= 1 
      hightlightSelection();
    }
  }

  hightlightSelection = function(){
    $('li').removeClass('highlighted')
    $('ul.search li:nth-child('+selected+')').addClass('highlighted')
  }

  $(o).keyup(findTrigger)
};

//paste
// remove on mouse click
// remove on window resize
// add sanitize method
// add 0 -1 filter
