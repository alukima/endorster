require 'depth_first_search'
require 'spec_helper'

describe DepthFirstSearch do
	describe "a simple graph with one connection" do
		let(:alice) { FactoryGirl.create(:user) }
		let(:bob) { FactoryGirl.create(:user) }
		let!(:rating) { FactoryGirl.create(:rating, :rater => alice, :ratee => bob) }

		it "finds the correct path" do
			DepthFirstSearch.find_paths(alice, bob).should == [[alice, bob]]
		end

		it "computes the score" do
			mock(PrioritizePaths).compute_score([[alice, bob]]) { 1337 }
			DepthFirstSearch.score(alice, bob).should == 1337
		end
	end
	
	describe "multiple paths between users" do
		let(:alice) { FactoryGirl.create(:user) }
		let(:bob) { FactoryGirl.create(:user) }
		let!(:rating_one) { FactoryGirl.create(:rating, :rater => alice, :ratee => bob) }
		let!(:rating_two) { FactoryGirl.create(:rating, :rater => alice, :ratee => bob) }

		it "finds the correct path only once" do
			DepthFirstSearch.find_paths(alice, bob).should == [[alice, bob]]
		end

	end
	
	describe "a cyclical graph with one inlet" do
		let(:alice) { FactoryGirl.create(:user) }
		let(:bob) { FactoryGirl.create(:user) }
		let(:carol) { FactoryGirl.create(:user) }
		let(:stranger) { FactoryGirl.create(:user) }
		
		let!(:rating_ab) { FactoryGirl.create(:rating, :rater => alice, :ratee => bob) }
		let!(:rating_bc) { FactoryGirl.create(:rating, :rater => bob, :ratee => carol) }
		let!(:rating_ca) { FactoryGirl.create(:rating, :rater => carol, :ratee => alice) }
		let!(:rating_sa) { FactoryGirl.create(:rating, :rater => stranger, :ratee => alice) }
		
		
		it "finds the correct path and no intersecting paths" do
			DepthFirstSearch.find_paths(stranger, alice).should == [[stranger, alice]]
		end

	end
end
