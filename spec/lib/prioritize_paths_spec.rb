require 'spec_helper'

describe PrioritizePaths do

	describe "two unconnected users" do
		let(:alice) { FactoryGirl.create(:user) }
		let(:bob) { FactoryGirl.create(:user) }
		
		it "computes the correct score" do
			PrioritizePaths.compute_score([[alice, bob]]).should == 0
		end
	end

	describe "a very simple graph" do
		let(:alice) { FactoryGirl.create(:user) }
		let(:bob) { FactoryGirl.create(:user) }
		let!(:rating) { FactoryGirl.create(:rating, :rater => alice, :ratee => bob, :value => 42, :weight => 50) }

		it "computes the correct score" do
			PrioritizePaths.compute_score([[alice, bob]]).should == 42
		end
	end
	
	describe "multiple ratings between the same two users" do
		let(:alice) { FactoryGirl.create(:user) }
		let(:bob) { FactoryGirl.create(:user) }
		let!(:rating_one) { FactoryGirl.create(:rating, :rater => alice, :ratee => bob, :value => 42, :weight => 50) }
		let!(:rating_two) { FactoryGirl.create(:rating, :rater => alice, :ratee => bob, :value => 48, :weight => 50) }

		it "computes the correct score" do
			PrioritizePaths.compute_score([[alice, bob]]).should == 90
		end
	end
	
	describe "a cycle with one inlet" do
		let(:alice) { FactoryGirl.create(:user) }
		let(:bob) { FactoryGirl.create(:user) }
		let(:carol) { FactoryGirl.create(:user) }
		let(:stranger) { FactoryGirl.create(:user) }
		
		let!(:rating_ab) { FactoryGirl.create(:rating, :rater => alice, :ratee => bob, :value => 42, :weight => 50) }
		let!(:rating_bc) { FactoryGirl.create(:rating, :rater => bob, :ratee => carol, :value => 37, :weight => 50) }
		let!(:rating_ca) { FactoryGirl.create(:rating, :rater => carol, :ratee => alice, :value => 26, :weight => 50) }
		let!(:rating_sa) { FactoryGirl.create(:rating, :rater => stranger, :ratee => alice, :value => 13, :weight => 50) }

		it "computes the correct score for stranger to alice" do
			PrioritizePaths.compute_score([[stranger, alice]]).should == 13
		end
	end
	
	describe "a cycle with one outlet" do
		let(:alice) { FactoryGirl.create(:user) }
		let(:bob) { FactoryGirl.create(:user) }
		let(:carol) { FactoryGirl.create(:user) }
		let(:stranger) { FactoryGirl.create(:user) }
		
		let!(:rating_ab) { FactoryGirl.create(:rating, :rater => alice, :ratee => bob, :value => 42, :weight => 50) }
		let!(:rating_bc) { FactoryGirl.create(:rating, :rater => bob, :ratee => carol, :value => 37, :weight => 50) }
		let!(:rating_ca) { FactoryGirl.create(:rating, :rater => carol, :ratee => alice, :value => 26, :weight => 50) }
		let!(:rating_sa) { FactoryGirl.create(:rating, :rater => alice, :ratee => stranger, :value => 13, :weight => 50) }

		it "computes the correct score for stranger to alice" do
			PrioritizePaths.compute_score([[alice, stranger]]).should == 13
		end
	end
end
