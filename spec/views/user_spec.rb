require 'spec_helper'

describe "users/show" do
	let(:alice) { FactoryGirl.create(:user, :username => "alice") }
	let(:bob) { FactoryGirl.create(:user) }
	let(:third_party) { FactoryGirl.create(:user) }

	describe "on the users own page" do
		before do
			stub(view).current_user { alice }
			assign(:user, alice)
			assign(:iratings, [])
			assign(:oratings, [])
			assign(:envelopes, [])
			render
		end

		it "welcomes the user" do
			rendered.should contain "Welcome, alice"
		end
	end

	describe "on another users page" do
		before do
			stub(view).current_user { bob }
			assign(:user, alice)
			assign(:iratings, [])
			assign(:oratings, [])
			assign(:envelopes, [])
			assign(:score, 12)
			assign(:backscore, -1)
			render
		end

		it "displays the users name and 'profile'" do
			rendered.should contain "alice's profile"
			rendered.should contain "Your score for alice is: 12"
			rendered.should contain "alice's score for you is: -1"
		end
	end

	describe "on another users page" do
		let(:rating) { FactoryGirl.create(:rating, :comment => "secret", :rater => alice, :ratee => bob) }

		before do
			stub(view).current_user { third_party }
			assign(:user, alice)
			assign(:iratings, [rating])
			assign(:oratings, [rating])
			assign(:envelopes, [])
			assign(:score, 12)
			assign(:backscore, -1)
			render
		end

		it "doesn't display comments on bob or alice's ratings" do
			rendered.should_not contain "secret"
		end
	end
end
