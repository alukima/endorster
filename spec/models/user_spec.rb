require 'spec_helper'

describe User do
	let(:user) { FactoryGirl.create(:user) }
	let(:alice) { FactoryGirl.create(:user, :username => "Alice") }
	let(:bob) { FactoryGirl.create(:user, :username => "Bob") }
	let(:carol) { FactoryGirl.create(:user) }
	let(:dave) { FactoryGirl.create(:user) }
	
	it "shows the username as param" do
		user.to_param.should == user.username
	end

	it "shows no trustees when user has not rated" do
		user.trustees.should == []
	end

	describe "with an unclosed rating" do
		let!(:rating) { FactoryGirl.create(:rating, :rater => user, :ratee => bob, :dateclosed => nil) }

		it "shows no trustees" do
			user.trustees.should == []
		end

		it "collects the correct trust score" do
			user.trust_for_user(bob).should == 0
		end
	end

	describe "with a rating" do
		let!(:rating) { FactoryGirl.create(:rating, :rater => user, :ratee => bob, :weight => 5, :value => 2) }

		it "shows the right trustees" do
			user.trustees.should == [bob]
		end

		it "collects the correct trust score" do
			user.trust_for_user(bob).should == 2
		end
	end

	
	

	describe "with two ratings" do
		let!(:rating1) { FactoryGirl.create(:rating, :rater => user, :ratee => bob, :weight => 5, :value => 2) }
		let!(:rating2) { FactoryGirl.create(:rating, :rater => user, :ratee => bob, :weight => 5, :value => 4) }

		it "shows the right trustees" do
			user.trustees.should == [bob]
		end

		it "collects the correct trust score" do
			user.trust_for_user(bob).should == 6
		end
	end
<<<<<<< HEAD
=======
	
	
	# Hipster backend testing
	describe "giving hipster cred" do
		it "gives hipster cred to the people who rated your target before you did" do
			rating = FactoryGirl.create(:rating, :rater => alice, :ratee => bob, :weight => 20, :value => 10)
			alice.give_hipster_cred(rating, user)

			HipsterRating.last.rater.should == user
			HipsterRating.last.ratee.should == alice
			HipsterRating.last.intermediary.should == bob
			HipsterRating.last.weight.should == "2"
			HipsterRating.last.value.should == "1"
			HipsterRating.last.dateclosed.should be_defined
		end
	end
	
	#BASIC HIPSTER CASES
	describe "simplest hipster case" do
		let!(:rating1) { FactoryGirl.create(:rating, :rater => alice, :ratee => bob, :weight => 51, :value => 50, ) }
		sleep(0.05) #rating2 must happen after rating1
		let!(:rating2) { FactoryGirl.create(:rating, :rater => carol, :ratee => bob, :weight => 51, :value => 50) }

		it "shows that carol has hipster trust for alice" do
			carol.trust_for_user(alice).should > 0
		end

		it "shows that alice DOES NOT have hipster trust for carol" do
			alice.trust_for_user(carol).should == 0
		end
		
		it "doesn't repeat the c -> b information when c looks at b" do
			carol.trust_for_user(bob).should == 50
		end
	end
	
	describe "backwards propagation of hipster cred, strictly back in time ( a -1> b -2> c <3- d )" do
		let!(:rating1) { FactoryGirl.create(:rating, :rater => alice, :ratee => bob, :weight => 51, :value => 50, ) }
		sleep(0.05)
		let!(:rating2) { FactoryGirl.create(:rating, :rater => bob, :ratee => carol, :weight => 51, :value => 50) }
		sleep(0.05)
		let!(:rating3) { FactoryGirl.create(:rating, :rater => dave, :ratee => carol, :weight => 51, :value => 50) }

		
		it "shows that dave has hipster trust for bob" do
			dave.trust_for_user(bob).should > 0
		end

		it "shows that dave has hipster trust for alice, which is less than his h.c. for bob" do
			dave.trust_for_user(alice).should > 0
			dave.trust_for_user(alice).should < dave.trust_for_user(bob)
		end
	end
	
	
	describe "backwards propagation of hipster cred, back and forth in time ( a -2> b -1> c <3- d )" do
		let!(:rating1) { FactoryGirl.create(:rating, :rater => bob, :ratee => carol, :weight => 51, :value => 50, ) }
		sleep(0.05)
		let!(:rating2) { FactoryGirl.create(:rating, :rater => alice, :ratee => bob, :weight => 51, :value => 50) }
		sleep(0.05)
		let!(:rating3) { FactoryGirl.create(:rating, :rater => dave, :ratee => carol, :weight => 51, :value => 50) }

		# should be the same as in the last case (i.e., we don't care about the order of 1 and 2, as long as they're before 3)
		it "shows that dave has hipster trust for bob" do
			dave.trust_for_user(bob).should > 0
		end

		it "shows that dave has hipster trust for alice, which is less than his h.c. for bob" do
			dave.trust_for_user(alice).should > 0
			dave.trust_for_user(alice).should < dave.trust_for_user(bob)
		end
	end
	
	describe "backwards propagation of hipster cred doesn't apply to the future ( a -3> b -1> c <2- d )" do
		let!(:rating1) { FactoryGirl.create(:rating, :rater => bob, :ratee => carol, :weight => 51, :value => 50, ) }
		sleep(0.05)
		let!(:rating2) { FactoryGirl.create(:rating, :rater => dave, :ratee => carol, :weight => 51, :value => 50) }
		sleep(0.05)
		let!(:rating3) { FactoryGirl.create(:rating, :rater => alice, :ratee => bob, :weight => 51, :value => 50) }

		# should be the same as in the last case (i.e., we don't care about the order of 1 and 2, as long as they're before 3)
		it "shows that dave has hipster trust for bob" do
			dave.trust_for_user(bob).should > 0
		end

		it "shows that dave DOES NOT have hipster trust for alice, since alice is a latecomer" do
			dave.trust_for_user(alice).should == 0
		end
	end
	
	#MORE COMPLEX HIPSTER CASES
	
	describe "zig-zagging hipster cred" do
		let!(:rating1) { FactoryGirl.create(:rating, :rater => alice, :ratee => bob, :weight => 51, :value => 50, ) }
		sleep(0.05)
		let!(:rating2) { FactoryGirl.create(:rating, :rater => carol, :ratee => bob, :weight => 51, :value => 50) }

		initial_hipster_cred_ca = carol.trust_for_user(alice)
		
		sleep(0.05)
		let!(:rating3) { FactoryGirl.create(:rating, :rater => alice, :ratee => bob, :weight => 51, :value => 50, ) }

		
		it "alice now has the same hipster cred for carol as carol for alice" do
			alice.trust_for_user(carol).should == carol.trust_for_user(alice)
		end
		
		sleep(0.05)
		let!(:rating4) { FactoryGirl.create(:rating, :rater => carol, :ratee => bob, :weight => 51, :value => 50, ) }

		it "carol now has double hipster cred for alice" do
			carol.trust_for_user(alice).should == 2*initial_hipster_cred_ca
		end
	end
	
	describe "uninterrupted repeated ratings (C-shaped case)" do
		let!(:rating1) { FactoryGirl.create(:rating, :rater => alice, :ratee => bob, :weight => 51, :value => 50, ) }
		sleep(0.05)
		let!(:rating2) { FactoryGirl.create(:rating, :rater => carol, :ratee => bob, :weight => 51, :value => 50) }

		initial_hipster_cred_ca = carol.trust_for_user(alice)
		
		sleep(0.05)
		let!(:rating3) { FactoryGirl.create(:rating, :rater => carol, :ratee => bob, :weight => 51, :value => 50, ) }

		
		it "carol now has double hipster cred for alice, and alice none for carol" do
			carol.trust_for_user(alice).should == 2*initial_hipster_cred_ca
			alice.trust_for_user(carol).should == 0
		end
		
		sleep(0.05)
		let!(:rating4) { FactoryGirl.create(:rating, :rater => alice, :ratee => bob, :weight => 51, :value => 50, ) }

		
		it "alice now has double hipster cred for carol, and carol still the same" do
			carol.trust_for_user(alice).should == 2*initial_hipster_cred_ca
			alice.trust_for_user(carol).should == 2*initial_hipster_cred_ca
		end

	end
	
>>>>>>> 54a71cdad6a2145360cc11b79f566435e9174064
end
