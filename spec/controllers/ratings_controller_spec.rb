require 'spec_helper'

describe RatingsController do
	describe "POST create" do
		let(:alice) { FactoryGirl.create(:user) }
		let(:bob) { FactoryGirl.create(:user) }
		let(:attributes) {
			{
			:rating => {
				:rater => bob.username,
				:weight => 456,
				:comment => "A comment"
				}
			}
		}

		it "creates a rating authorization" do
			stub(subject).current_user { alice }
			post :create, attributes

			Rating.last.ratee.should == alice
			Rating.last.rater.should == bob
			Rating.last.weight.should == 456
			Rating.last.dateclosed.should == nil
			Rating.last.value.should == 0
		end
	end

	describe "PUT update" do
		let(:bob) { FactoryGirl.create(:user) }
		let!(:rating) { FactoryGirl.create(:rating, :rater => bob, :weight => 5000, :value => 0, :dateclosed => nil) }
		let(:attributes) {
			{
			:id => rating.to_param,
			:rating => {
				:value => 123
				}
			}
		}

		it "saves the rating" do
			stub(subject).current_user { bob }
			put :update, attributes

			Rating.last.dateclosed.should_not == nil
			Rating.last.value.should == 123
		end
	end
end
