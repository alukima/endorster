FactoryGirl.define do
	factory :user do
		sequence(:username) { |n| "User#{n}" }	
		sequence(:password) { |n| "password#{n}" }	
		sequence(:password_confirmation) { |n| "password#{n}" }	
	end

	factory :rating do
		association :rater, factory: :user
		association :ratee, factory: :user
		dateopened DateTime.now
		dateclosed DateTime.now
		weight 50
		value 33
	end
end
