# Load the rails application
require File.expand_path('../application', __FILE__)

# Initialize the rails application
RailsinstallerDemo::Application.initialize!




RailsinstallerDemo::Application.configure do


config.action_mailer.delivery_method = :smtp   # ???

# I do care if the mailer can't send
config.action_mailer.raise_delivery_errors = true

# Include your app's configuration here:
config.action_mailer.smtp_settings = {
	:enable_starttls_auto => true,
	:address => 'smtp.gmail.com',
	:port => 587,
	:domain => 'endorster.net',
	:authentication => :plain,
	:user_name => 'noreply@endorster.net',
	:password => 'uniquemultiplier'
}


end
