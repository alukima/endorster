RailsinstallerDemo::Application.routes.draw do
	#Reformed url scheme
	root :to => 'welcome#index'
	
	match 'user/:username/activity' => 'users#activity'

	match 'login' => 'user_sessions#new', :via => :get
	match 'login', :to => 'user_sessions#create', :via => :post
	match 'logout' => 'user_sessions#destroy', :as => :logout
	#resources :sessions, only: [:new, :create, :destroy]
	#resources :user_sessions
	
	match 'about' => 'welcome#about'
	match 'help' => 'welcome#help'
	match 'api/docs' => 'welcome#api_docs'
	match 'legal' => 'welcome#legal'

	match 'signup' => 'users#new', :via => :get
	match 'signup' => 'users#create', :via => :post
	
	match 'user/:id' => 'users#show', :as => :user
	
	match 'mark_read' => 'users#mark_read'
  match 'dismiss' => 'users#dismiss_notification', :as => :dismiss

	match 'dynamic_score' => 'users#dynamic_score'
	match ':username/notifications' => 'users#notifications'
	match 'public_events/:id' => 'users#public_events'
	match 'dynamic_search' => 'users#dynamic_search'

	match 'home' => 'users#home'
	
	match 'profile' => 'connections#index', :as => :connections, :via=>:get
	match 'profile' => 'connections#create', :as => :connections_create, :via=>:post
	match 'profile' => 'users#update_description', :as => :update_description, :via=>:put

	match 'profile/new' => 'connections#new', :as => :connections_new
	
	match 'profile/connect' => 'users#associate', :via => :get
	match 'profile/connect' => 'users#request_association', :via => :post
	match 'profile/connect/:id' => 'users#answer_association', :via => :get
	match 'profile/connect/:id/confirm' => 'users#confirm_association', :via => :get
	match 'profile/connect/:id/reject' => 'users#reject_association', :via => :get

	
	match 'activate/:code' => 'users#activate', :as => :activate, :via => :get
	match 'activate/:code' => 'users#finalize_activation', :as => :activate, :via => :put


	match 'invite' => 'rating_invitations#new', :via => :get
	match 'invite/new' => 'rating_invitations#create', :via => :post
	match 'invitation/:id' => 'rating_invitations#show'
	match 'invitation/:id/regenerate' => 'rating_invitations#regenerate', :as => :regenerate_invitation
	match 'invitation/:id/delete' => 'rating_invitations#delete', :as => :delete_invitation

	match '/i/:id' => 'rating_invitations#show_unique', :as => :i
	match '/accept/:id' => 'ratings#accept_invitation', :as => :accept
	match 'invite/email' => 'rating_invitations#email_invite', :via => :get
	match 'invite/email' => 'rating_invitations#send_email_invite', :via => :post

 
	
	match 'rate' => 'users#confirm_ratings'
	match 'rate/:id' => 'ratings#rate', :via => :get, :as => :rating
	match 'rate/:id' => 'ratings#update', :via => :put
	
	match 'share' => 'links#index', :via => :get
	match 'share' => 'links#create', :via => :post
	match 's/:id' => 'links#show'
	match 's/:id/delete' => 'links#destroy'
	
	match 'settings' => 'users#show_settings', :via => :get
	match 'settings' => 'users#update', :via => :put, :as => :update_settings
	match 'settings/v' => 'users#verification_email'
	match 'v/:code' => 'users#verify'
	
	match 'api/secure' => 'api#api_secure', :as => :api_secure, :via => :POST
	match 'api/score' => 'api#api_score', :as => :api_score, :via => :GET
	match 'api/signup' => 'api#api_signup', :as => :api_signup, :via => :POST
	match 'api/invitation' => 'api#api_invitation', :as => :api_invitation, :via => :GET
	match 'api/invite' => 'api#api_invite', :as => :api_invite, :via => :POST
	match 'api/rate' => 'api#api_rate', :as => :api_rate, :via => :POST
	match 'api/set_public_key' => 'api#api_set_public_key', :as => :api_set_public_key, :via => :POST
	match 'api/next_sequence' => 'api#api_next_sequence', :as => :api_next_sequence, :via => :GET
end
